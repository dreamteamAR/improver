package com.improver.app.sessions.manager.sessions

class NormalSessionProcessor(override val onSessionProcessorEventListener: OnSessionProcessorEventListener): SessionProcessor {

    var startTime: Long = System.currentTimeMillis()
    var started: Boolean = false
    override fun start() {
        startTime  = System.currentTimeMillis()

    }

    override fun getTime(): Long {
        return System.currentTimeMillis() - startTime
    }

    override fun update() {
        if(!started) {
            started = true
            onSessionProcessorEventListener.onSessionStart()
        }
    }
}