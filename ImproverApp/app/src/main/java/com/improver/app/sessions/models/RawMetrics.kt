package com.improver.app.sessions.models

import com.improver.app.common.models.Metric
import com.improver.app.common.services.ObjectService
import com.improver.app.synchronization.persistance.Request
import java.io.ByteArrayOutputStream
import java.io.OutputStreamWriter
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.zip.GZIPOutputStream

class RawMetrics(val metric: String, metrics: List<Metric>) {
    val headers: List<String> = metrics[0].values.keys.toList()
    val values: ByteArray = gzip(metrics.map {
        metric -> metric.values.map {
        it.value
        }
    })

    fun toRequest(sessionId: UUID): Request {
        val s = ObjectService.writeValueAsString(this)
        return Request(null, "POST", "/sessions/$sessionId/raw-data", s)
    }

    @Throws(Exception::class)
    private fun gzip(metrics: List<List<Double>>): ByteArray {
        val s = ObjectService.writeValueAsString(metrics)
        val bos = ByteArrayOutputStream()
        val gzip = GZIPOutputStream(bos)
        val osw = OutputStreamWriter(gzip, StandardCharsets.UTF_8)
        osw.write(s)
        osw.close()
        return bos.toByteArray()
    }
}