package com.improver.app.sessions.manager

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.widget.Toast
import com.improver.app.AppActivity
import com.improver.app.athletes.persistance.AthleteEntity
import com.improver.app.common.models.Metric
import com.improver.app.sensors.interfaces.OnSensorDataListener
import com.improver.app.sensors.interfaces.SensorManager
import com.improver.app.sessions.interfaces.OnSessionChangeListener
import com.improver.app.sessions.interfaces.SessionManager
import com.improver.app.sessions.manager.disksaver.DiskSaver
import com.improver.app.sessions.manager.servicesaver.ServiceSaver
import com.improver.app.sessions.manager.sessions.CooperSessionProcessor
import com.improver.app.sessions.manager.sessions.NormalSessionProcessor
import com.improver.app.sessions.manager.sessions.OnSessionProcessorEventListener
import com.improver.app.sessions.manager.sessions.SessionProcessor
import com.improver.app.sessions.models.Session
import com.improver.app.sessions.models.SessionDetails
import com.improver.app.sessions.models.SessionState
import com.improver.app.sessions.models.SessionType
import com.improver.app.synchronization.interfaces.Synchronizer
import java.lang.RuntimeException
import java.text.SimpleDateFormat
import java.util.*

class SynchronizedSessionManager(private val activity: Activity, private val athleteEntity: AthleteEntity, private val sensorManager: SensorManager, private val synchronizer: Synchronizer)
    :
        SessionManager,
        OnSensorDataListener,
        OnSessionProcessorEventListener
{
    override fun deinit() {
        diskSaver.stop()
        diskSaverThread.join()
    }

    private var session: Session? = null;
    private var sessionProcessor: SessionProcessor? = null
    private var saveOnDisk: Boolean = false
    private var currentSessionId: UUID = UUID.randomUUID()
    private var diskSaver: DiskSaver = DiskSaver(athleteEntity.athleteId)
    private var diskSaverThread = Thread(diskSaver)
    private var serviceSaver: ServiceSaver? = null
    private var serviceSaverThread: Thread? = null

    override var sessionType: SessionType = SessionType.NORMAL

    private var buffer: MutableList<Metric> = LinkedList()


    override fun communicateEvent(string: String) {
        (activity as AppActivity).speekText(string)
    }



    override fun onSensorData(metric: Metric) {
        sessionProcessor?.update()
        timeChange()
        if(sessionState == SessionState.ACTIVE) {
            if(saveOnDisk) {
                diskSaver.queue.add(metric)
            } else {
                serviceSaver?.queue?.add(metric)
            }

        }

    }


    private fun timeChange() {
        onSessionChangeListeners.forEach { it.onTimeChange(sessionProcessor?.getTime()?: 0) }
    }

    override fun onSessionStart() {
       changeSessionState(SessionState.ACTIVE)
    }

    override fun onSessionEnd() {
        stop()
    }

    override var sessionState: SessionState = SessionState.INACTIVE
    override var onSessionChangeListeners: MutableList<OnSessionChangeListener> = LinkedList()

    override fun endSession() {
        stop()
    }

    private fun stop() {
        if(sessionState == SessionState.ACTIVE || sessionState == SessionState.PREPARATION) {
            if(!saveOnDisk) {
                session!!.endTime = System.currentTimeMillis()
                synchronizer.addRequest(session!!.saveRequest())
                serviceSaver?.stop()
                serviceSaverThread?.join()
            } else {
                diskSaver.stop()
                diskSaverThread.join()
            }
            sessionProcessor?.stop()
            sensorManager.stopStreaming()
            changeSessionState(SessionState.INACTIVE)
        }
    }

    override fun getTime(): Long {
        return sessionProcessor?.getTime()?: 0
    }

    @SuppressLint("SimpleDateFormat")
    override fun startSession() {
        currentSessionId = UUID.randomUUID()
        changeSessionState(SessionState.PREPARATION)
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        saveOnDisk = sharedPref.getBoolean("saveOnDisk", false)
        try {
            sensorManager.startStreaming(saveOnDisk)
        } catch (e: RuntimeException) {
            Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
            return
        }
        sessionProcessor = when(sessionType) {
            SessionType.NORMAL -> NormalSessionProcessor(this)
            SessionType.COOPER -> CooperSessionProcessor(activity, this)
        }
        sessionProcessor?.start()

        if(!saveOnDisk) {
            session = Session(currentSessionId, athleteEntity.athleteId, System.currentTimeMillis(), SessionDetails(sessionType))
            synchronizer.addRequest(session!!.createRequest())
            serviceSaver = ServiceSaver(synchronizer, currentSessionId)
            serviceSaverThread = Thread(serviceSaver)
            serviceSaverThread?.start()
        } else {
            diskSaver.prefix =  SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Date())
            diskSaverThread.start()
        }



    }

    private fun changeSessionState(state: SessionState) {
        sessionState = state
        onSessionChangeListeners.forEach { it.onSessionStateChange(state) }
    }
}