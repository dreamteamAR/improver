package com.improver.app.sensors.interfaces

interface SensorManagerProvider {
    fun getSensorManager(): SensorManager
}