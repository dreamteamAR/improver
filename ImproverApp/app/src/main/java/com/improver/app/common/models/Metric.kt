package com.improver.app.common.models

data class Metric(var metric: String, var values: MutableMap<String, Double>)