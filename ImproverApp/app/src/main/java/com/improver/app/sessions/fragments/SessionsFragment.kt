package com.improver.app.sessions.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.improver.app.AppActivity

import com.improver.app.R
import com.improver.app.common.Tile
import com.improver.app.sensors.fragments.SensorsFragment
import com.improver.app.sensors.interfaces.OnSensorInformationChangeListener
import com.improver.app.sensors.interfaces.SensorManager
import com.improver.app.sensors.interfaces.SensorManagerProvider
import com.improver.app.sessions.interfaces.SessionManager
import com.improver.app.sessions.interfaces.SessionManagerProvider


class SessionsFragment : androidx.fragment.app.Fragment(), OnSensorInformationChangeListener, TimeFragment.SessionEndClickListener {

    override fun onSessionEndClick() {
        sessionManager?.onSessionChangeListeners?.remove(timeFragment)
        sessionManager?.endSession()
    }

    private var sessionManager: SessionManager? = null
    private var sensorManager: SensorManager? = null

    private var startSessionTile: Tile? = null
    private var sensorsTile: Tile? = null
    private var sessionTypeTile: Tile? = null
    private var timeFragment: TimeFragment = TimeFragment(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_sessions, container, false)

        val sensors: View = view.findViewById(R.id.sensors_tile)
        sensors.setOnClickListener{
            (activity as AppActivity).changeFragmentPushToBackStack(SensorsFragment(), "Sensors")
        }
        startSessionTile = createStartSessionTile(view)
        sensorsTile = createSensorsTile(view)
        sessionTypeTile = createSessionTypeTile(view)

        view.findViewById<View>(R.id.session_tile).setOnClickListener{
            if(isReadyForSession()) {
                val fragment = TimeFragment(this)
                (activity as AppActivity).changeFragmentPushToBackStack(fragment, "Session state")
                sessionManager?.onSessionChangeListeners?.add(fragment)
                sessionManager?.startSession()
            } else {
                Toast.makeText(context, "Cannot start session without connected sensor", Toast.LENGTH_SHORT).show()
            }
        }

        view.findViewById<View>(R.id.type_tile).setOnClickListener {
            (activity as AppActivity).changeFragmentPushToBackStack(SessionTypeItemFragment(), "Session type")
        }

        updateTiles()
        return view
    }

    private fun createStartSessionTile(view: View): Tile {
        return Tile(view.findViewById(R.id.session_tile), view.findViewById(R.id.start_session_image) ,view.findViewById(R.id.session_description))
    }

    private fun createSensorsTile(view: View): Tile {
        return Tile(view.findViewById(R.id.sensors_tile), view.findViewById(R.id.sensors_image) ,view.findViewById(R.id.sensors_description))
    }

    private fun createSessionTypeTile(view: View): Tile {
        return Tile(view.findViewById(R.id.type_tile), view.findViewById(R.id.type_image), view.findViewById(R.id.type_description))
    }

    private fun isReadyForSession(): Boolean {
        if(sensorManager!!.getConnectedSensors().isEmpty())
            return false
        return true
    }

    private fun updateSessionTile() {
        activity?.runOnUiThread{
            if(!isReadyForSession()) {
                startSessionTile?.setColor(context!!, R.drawable.shadow_gray)
            } else {
                startSessionTile?.setColor(context!!, R.drawable.shadow_blue_button)
            }
        }

    }

    private fun updateSensorsTile() {
        val connectedSensorsCount = sensorManager!!.getConnectedSensors().size
        activity?.runOnUiThread {
            if (connectedSensorsCount < 1) {
                sensorsTile?.setDescription("No sensors connected.")
            } else {
                sensorsTile?.setDescription("Currently you have connected $connectedSensorsCount sensors.")
            }
        }
    }


    private fun updateTypeTile() {
        activity?.runOnUiThread {
            sessionTypeTile?.setDescription("Currently selected type is ${sessionManager?.sessionType.toString().toLowerCase().capitalize()}.")
        }
    }
    private fun updateTiles() {
        updateSensorsTile()
        updateSessionTile()
        updateTypeTile()
    }

    override fun onSensorStateChanged(bluetoothAddress: String) {
        updateTiles()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sessionManager = when (context) {
            is SessionManager -> context
            is SessionManagerProvider -> context.getSessionManager()
            else -> throw RuntimeException("$context must implement SessionManager or SessionManagerProvider")
        }
        sensorManager = when (context) {
            is SensorManager -> context
            is SensorManagerProvider -> context.getSensorManager()
            else -> throw RuntimeException("$context must implement SensorManager or SensorManagerProvider")
        }
        sensorManager?.onSensorInformationChangeListeners?.add(this)
    }

    override fun onDetach() {
        super.onDetach()
        sensorManager?.onSensorInformationChangeListeners?.remove(this)
        sessionManager = null
        sensorManager = null

    }
}