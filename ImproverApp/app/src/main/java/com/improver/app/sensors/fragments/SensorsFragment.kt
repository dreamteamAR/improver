package com.improver.app.sensors.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.improver.app.R
import com.improver.app.common.layout.interfaces.FullscreenFragmentActivity
import java.lang.RuntimeException


class SensorsFragment : androidx.fragment.app.Fragment() {

    var fullscreenFragmentActivity: FullscreenFragmentActivity? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sensors, container, false)
        view.findViewById<View>(R.id.paired_devices).setOnClickListener {
            fullscreenFragmentActivity?.showFragment(PairedSensorFragment(), "Paired sensors")
        }
        view.findViewById<View>(R.id.discover_devices).setOnClickListener {
            fullscreenFragmentActivity?.showFragment(DiscoveredSensorFragment(), "Discovered sensors")
        }
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fullscreenFragmentActivity = when(context is FullscreenFragmentActivity) {
            true -> context
            else -> throw RuntimeException("$context must implement FullscreenFragmentActivity")
        }
    }


    override fun onDetach() {
        super.onDetach()
        fullscreenFragmentActivity = null
    }
}
