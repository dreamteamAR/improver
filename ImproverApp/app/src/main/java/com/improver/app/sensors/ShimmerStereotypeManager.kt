package com.improver.app.sensors

import com.shimmerresearch.android.Shimmer
import com.shimmerresearch.driver.ShimmerDevice
import java.lang.RuntimeException


class ShimmerStereotypeManager {
    private val timeSensors: List<String> = listOf("System_Timestamp", "Timestamp")
    private var stereotypeMap: MutableMap<Stereotype, ShimmerDevice?> = mutableMapOf(
        *(Stereotype.values().map{ Pair<Stereotype, ShimmerDevice?>(it, null)}.toTypedArray())
    )

    fun clear() {
        stereotypeMap  = mutableMapOf(
            *(Stereotype.values().map{ Pair<Stereotype, ShimmerDevice?>(it, null)}.toTypedArray())
        )
    }

    fun getStereotype(bluetoothAddress: String): String {
        val appliedStereotypes = stereotypeMap.filter { it.value?.btConnectionHandle == bluetoothAddress }.keys
        if(appliedStereotypes.isNotEmpty()) {
            return appliedStereotypes.toList()[0].name.toLowerCase()
        }
        return "unknown"
    }

    fun assignStereotype(shimmerDevice: ShimmerDevice) {
        val enabledSensors = getEnabledSensors(shimmerDevice)
        if(!enabledSensors.containsAll(timeSensors)) {
            throw RuntimeException("${timeSensors.joinToString(" and ")} have to be enabled!")
        }
        if(tryToApplyStereotype(shimmerDevice, enabledSensors, Stereotype.ECG))
            return
        if(tryToApplyStereotype(shimmerDevice, enabledSensors, Stereotype.RESP))
            return
        if(tryToApplyStereotype(shimmerDevice, enabledSensors, Stereotype.GSR))
            return
        if(tryToApplyStereotype(shimmerDevice, enabledSensors, Stereotype.IMU))
            return
        throw RuntimeException("Unable to match ${shimmerDevice.mShimmerUserAssignedName} to any known stereotype or all applicable already taken.")
    }

    private fun tryToApplyStereotype(shimmerDevice: ShimmerDevice, enabledSensors: List<String>, stereotype: Stereotype): Boolean {
        if(enabledSensors.containsAll(stereotype.requiredSensors)) {
            if(stereotypeMap[stereotype] == null) {
                stereotypeMap[stereotype] = shimmerDevice
                return true
            }
        }
        return false
    }

    private fun getEnabledSensors(shimmerDevice: ShimmerDevice): List<String> {
        val shimmer: Shimmer = shimmerDevice as Shimmer
        return shimmer.sensorMap
            .filter { shimmerDevice.isVerCompatibleWithAnyOf(it.value!!.mSensorDetailsRef.mListOfCompatibleVersionInfo) && it.value.isEnabled }
            .map{it.value.mSensorDetailsRef.mGuiFriendlyLabel}
    }


    enum class Stereotype(val requiredSensors: List<String>) {
        IMU(listOf("Gyroscope")),
        ECG(listOf("ECG")),
        RESP(listOf("Respiration")),
        GSR(listOf("GSR"))
    }
}