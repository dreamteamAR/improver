package com.improver.app

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.improver.app.athletes.persistance.AthleteDao
import com.improver.app.athletes.persistance.AthleteEntity
import com.improver.app.athletes.persistance.AthleteGenderConverter

import com.improver.app.synchronization.persistance.Request
import com.improver.app.synchronization.persistance.RequestDao

@Database(entities = arrayOf(Request::class, AthleteEntity::class), version = 3)
@TypeConverters(AthleteGenderConverter::class)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract fun requestDao(): RequestDao
    abstract fun athleteDao(): AthleteDao
}