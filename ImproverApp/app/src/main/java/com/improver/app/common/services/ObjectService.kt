package com.improver.app.common.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

object ObjectService : ObjectMapper() {
    init {
        this.registerModule(KotlinModule())
    }
}