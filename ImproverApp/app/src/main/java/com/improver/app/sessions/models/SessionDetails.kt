package com.improver.app.sessions.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class SessionDetails(
    var type: SessionType = SessionType.NORMAL,
    var distance: Int? = null
) : Parcelable