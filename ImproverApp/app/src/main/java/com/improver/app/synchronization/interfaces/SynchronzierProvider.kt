package com.improver.app.synchronization.interfaces

interface SynchronzierProvider {
    fun getSynchronizer(): Synchronizer
}