package com.improver.app.sensors.interfaces

import com.improver.app.common.models.Metric

interface OnSensorDataListener {
    fun onSensorData(metric: Metric)
}