package com.improver.app.sessions.manager.sessions

interface OnSessionProcessorEventListener {
    fun onSessionStart()
    fun communicateEvent(string: String)
    fun onSessionEnd()
}