package com.improver.app.sessions.models

import android.os.Parcelable
import com.improver.app.common.services.ObjectService
import com.improver.app.synchronization.persistance.Request

import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Session(var id: UUID, var athleteId: String, var startTime: Long, var details: SessionDetails, var endTime: Long? = null): Parcelable {
    fun createRequest(): Request {
        return Request(null, "POST","/sessions", ObjectService.writeValueAsString(this))
    }

    fun saveRequest(): Request {
        return Request(null, "PUT","/sessions/$id", ObjectService.writeValueAsString(this))
    }
}