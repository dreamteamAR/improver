package com.improver.app.sessions.manager.sessions

interface SessionProcessor {
    val onSessionProcessorEventListener: OnSessionProcessorEventListener
    fun start()
    fun getTime(): Long
    fun isReady(): Boolean {
        return true
    }
    fun update() {}
    fun stop() {}
}