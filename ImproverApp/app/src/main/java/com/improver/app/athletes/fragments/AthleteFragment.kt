package com.improver.app.athletes.fragments

import android.content.Context
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.improver.app.R
import com.improver.app.athletes.adapters.AthleteRecyclerViewAdapter
import com.improver.app.athletes.persistance.AthleteDao
import com.improver.app.athletes.persistance.AthleteEntity
import com.improver.app.athletes.services.AthleteService

import java.util.*

class AthleteFragment(private val athleteDao: AthleteDao) : androidx.fragment.app.Fragment() {

    private var listener: OnListFragmentInteractionListener? = null

    private var athleteData: MutableList<AthleteEntity> = LinkedList()

    private var mAdapter: AthleteRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getAthletes()
    }

    override fun onResume() {
        super.onResume()
        getAthletes()
    }

    private fun getAthletes() {
        AthleteService.getAthletes(
                { athletes ->
                this@AthleteFragment.athleteData.clear()
                    athletes?.forEach{
                        var athlete = athleteDao.findByAthleteId(it.id!!)
                        if(athlete != null ){
                            athlete.firstName = it.firstName
                            athlete.lastName = it.lastName
                            athlete.birthDate = it.birthDate
                            athlete.gender = it.gender
                            athlete.height = it.height
                            athlete.weight = it.weight
                            athlete.imageUrl = it.imageUrl
                            athleteDao.updateAthlete(athlete)
                        } else {
                            athlete = AthleteEntity(null, it.id, it.firstName, it.lastName, it.imageUrl, it.birthDate, it.weight, it.height, it.gender)
                            athlete.id = athleteDao.insert(athlete)
                        }
                        athleteData.add(athlete)
                    }
                    val existingIds = athleteData.map{ it.id }
                    athleteDao.findAll().filter { !existingIds.contains(it.id)}.forEach{
                        athleteDao.delete(it)
                    }



                this@AthleteFragment.activity?.runOnUiThread {
                    this@AthleteFragment.mAdapter?.notifyDataSetChanged()
                }
            },
            {
                athleteData.clear()
                athleteData.addAll(athleteDao.findAll())
                this@AthleteFragment.activity?.runOnUiThread {
                    Toast.makeText(activity, "Failed to fetch athletes", Toast.LENGTH_SHORT).show()
                    this@AthleteFragment.mAdapter?.notifyDataSetChanged()
                }
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_athlete_list, container, false)
        val recyclerView: androidx.recyclerview.widget.RecyclerView = view.findViewById(R.id.athletesRecyclerView)
        view.findViewById<FloatingActionButton>(R.id.addAthlete).setOnClickListener {
            listener?.onNewAthleteClick()
        }
        view.findViewById<FloatingActionButton>(R.id.refreshButton).setOnClickListener {
            getAthletes()
        }
        this@AthleteFragment.mAdapter =
            AthleteRecyclerViewAdapter(athleteData, listener)
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        recyclerView.adapter = this.mAdapter
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: AthleteEntity?)
        fun onNewAthleteClick()
    }

}
