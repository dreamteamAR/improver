package com.improver.app.athletes.persistance

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.improver.app.athletes.models.Gender
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class AthleteEntity(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo val athleteId: String,
    @ColumnInfo var firstName: String,
    @ColumnInfo var lastName: String,
    @ColumnInfo var imageUrl: String,
    @ColumnInfo var birthDate: String,
    @ColumnInfo var weight: Int,
    @ColumnInfo var height: Int,
    @ColumnInfo var gender: Gender

): Parcelable

