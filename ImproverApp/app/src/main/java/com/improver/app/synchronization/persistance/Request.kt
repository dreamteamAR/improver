package com.improver.app.synchronization.persistance

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Request (
    @PrimaryKey(autoGenerate = true) val id: Long?,
    @ColumnInfo val method: String,
    @ColumnInfo val endpoint: String,
    @ColumnInfo val content: String
)