package com.improver.app.athletes.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.improver.app.R
import com.improver.app.athletes.models.Athlete


class AthleteInformationsFragment : androidx.fragment.app.Fragment() {

    private var athlete: Athlete? = null

    companion object {
        fun newInstance(athlete: Athlete): AthleteInformationsFragment {
            val obj = AthleteInformationsFragment()
            obj.athlete = athlete
            return obj
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.athlete_informations_fragment, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
    }

}
