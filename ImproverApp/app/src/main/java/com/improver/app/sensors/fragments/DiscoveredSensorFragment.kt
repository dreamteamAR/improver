package com.improver.app.sensors.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.improver.app.R
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.animation.ValueAnimator
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.widget.Toast
import com.improver.app.sensors.BasicBluetoothDiscoverer
import com.improver.app.sensors.models.SensorInformations
import com.improver.app.sensors.adapters.SensorRecyclerViewAdapter
import com.improver.app.sensors.interfaces.OnBluetoothDiscoveryEventListener
import com.improver.app.sensors.interfaces.OnSensorInformationChangeListener
import com.improver.app.sensors.interfaces.SensorManager
import com.improver.app.sensors.interfaces.SensorManagerProvider
import java.util.*

class DiscoveredSensorFragment : androidx.fragment.app.Fragment(), OnBluetoothDiscoveryEventListener,
    OnSensorInformationChangeListener {


    private var bluetoothDiscoverer: BasicBluetoothDiscoverer? = null

    private var sensorManager: SensorManager? = null

    private var sensorsData: MutableList<SensorInformations> = LinkedList()

    private var recyclerViewAdapter: SensorRecyclerViewAdapter? = null

    private var animator: ObjectAnimator? = null

    override fun onBluetoothDiscoveryStart() {
        sensorsData.clear()
        activity?.runOnUiThread {
            recyclerViewAdapter?.notifyDataSetChanged()
        }
        animator?.start()
    }

    override fun onDiscoveryFail() {
        Toast.makeText(activity?.applicationContext, "Bluetooth is not enabled or not available", Toast.LENGTH_SHORT).show()
    }

    override fun onBluetoothDiscoveryEnd() {
        animator?.end()
    }

    override fun onBluetoothDiscoveredDevice(device: BluetoothDevice) {
        sensorsData.add(
            SensorInformations(
                activity!!,
                device.name.orEmpty(),
                device.address,
                SensorInformations.State.DISCONNECTED
            )
        )
        activity?.runOnUiThread {
            recyclerViewAdapter?.notifyDataSetChanged()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recyclerViewAdapter = SensorRecyclerViewAdapter(sensorsData, sensorManager, context!!)

    }

    private fun createAnimator(view: View) {
        val rotation = PropertyValuesHolder.ofFloat(View.ROTATION, 1440.0f)
        animator = ObjectAnimator.ofPropertyValuesHolder(view, rotation)
        animator?.repeatCount = ValueAnimator.INFINITE
        animator?.duration = 5000
    }
    
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_discoveredsensors_list, container, false)
        val recycler: androidx.recyclerview.widget.RecyclerView = view.findViewById(R.id.list)
        val discoveringButton: View = view.findViewById(R.id.discoveringButton)
        discoveringButton.setOnClickListener {
            when(bluetoothDiscoverer!!.isDiscovering()) {
                true -> bluetoothDiscoverer!!.endDiscovery()
                false -> bluetoothDiscoverer!!.startDiscovery()
            }
        }
        createAnimator(discoveringButton)
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        recycler.adapter = recyclerViewAdapter

        return view
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        sensorManager = when(context) {
            is SensorManager -> context
            is SensorManagerProvider -> context.getSensorManager()
            else -> throw RuntimeException("$context must implement SensorManager or SensorManagerProvider")
        }
        sensorManager?.onSensorInformationChangeListeners?.add(this)
        bluetoothDiscoverer = BasicBluetoothDiscoverer(activity)
        bluetoothDiscoverer!!.onBluetoothDiscoveryEventListeners.add(this)
        bluetoothDiscoverer!!.register()
    }

    override fun onDetach() {
        sensorManager?.onSensorInformationChangeListeners?.remove(this)
        bluetoothDiscoverer!!.unregister()
        bluetoothDiscoverer!!.onBluetoothDiscoveryEventListeners.remove(this)
        super.onDetach()
    }

    override fun onSensorStateChanged(bluetoothAddress: String) {
        refreshDevice(bluetoothAddress)
    }

    private fun refreshDevice(bluetoothAddress: String) {
        if(sensorManager!!.getConnectedSensors().map { it.address }.filter { it == bluetoothAddress }.isNotEmpty()) {
            sensorsData.filter { it.address == bluetoothAddress && it.state != SensorInformations.State.CONNECTED }.forEach{
                it.state = SensorInformations.State.CONNECTED
                recyclerViewAdapter?.notifyItemChanged(sensorsData.indexOf(it))
            }
        } else {
            sensorsData.filter { it.address == bluetoothAddress && it.state != SensorInformations.State.DISCONNECTED }.forEach{
                it.state = SensorInformations.State.DISCONNECTED
                recyclerViewAdapter?.notifyItemChanged(sensorsData.indexOf(it))
            }
        }
    }

}
