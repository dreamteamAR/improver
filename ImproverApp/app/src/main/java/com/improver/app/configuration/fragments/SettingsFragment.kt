package com.improver.app.configuration.fragments


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch

import com.improver.app.R

class SettingsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
        view.findViewById<Switch>(R.id.save_on_disk_switch).isChecked = sharedPref.getBoolean("saveOnDisk", false)
        view.findViewById<Switch>(R.id.save_on_disk_switch).setOnCheckedChangeListener { _, isChecked ->
                val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
                val editor = sharedPref?.edit()
                editor?.putBoolean("saveOnDisk", isChecked)
                editor?.apply()
        }
        return view
    }


}
