package com.improver.app.sessions.manager.disksaver

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

class FileHolder(private val file: File) {
    private val fw = FileWriter(file.absoluteFile)
    private val bw = BufferedWriter(fw)
    var firstWrite: Boolean = true

    fun saveData(data: Map<String, Double>) {
        if(firstWrite) {
            data.keys.forEach {
                bw.write("$it,")
            }
            bw.newLine()
            firstWrite = false
        }
        data.values.forEach {
            bw.write("$it,")
        }
        bw.newLine()
    }

    fun close() {
        bw.flush()
        bw.close()
        fw.close()
    }
}