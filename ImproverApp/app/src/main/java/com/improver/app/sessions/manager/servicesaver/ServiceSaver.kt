package com.improver.app.sessions.manager.servicesaver

import com.improver.app.common.models.Metric
import com.improver.app.sessions.models.RawMetrics
import com.improver.app.synchronization.interfaces.Synchronizer
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

class ServiceSaver(private val synchronizer: Synchronizer, var sessionId: UUID) : Runnable {

    var running: Boolean = false
    var valuesMap: MutableMap<String, MutableList<Metric>> = TreeMap()
    override fun run() {
        running = true
        while(running) {
            when(val metric = queue.poll()) {
                null -> Thread.sleep(10)
                else -> {
                    if(!valuesMap.containsKey(metric.metric)) {
                        valuesMap[metric.metric] = LinkedList()
                    }
                    valuesMap[metric.metric]!!.add(metric)
                    if(valuesMap[metric.metric]!!.size > 512){
                        val packet = RawMetrics(metric.metric, valuesMap[metric.metric]!!)
                        synchronizer.addRequest(packet.toRequest(sessionId))
                        valuesMap[metric.metric] = LinkedList()
                    }
                }
            }
        }
    }

    val queue: ConcurrentLinkedQueue<Metric> = ConcurrentLinkedQueue()

    fun stop() {
        running = false
    }
}