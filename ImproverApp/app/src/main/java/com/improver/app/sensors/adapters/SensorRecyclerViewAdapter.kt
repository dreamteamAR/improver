package com.improver.app.sensors.adapters

import android.content.Context
import android.os.Handler
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.improver.app.R
import com.improver.app.sensors.interfaces.OnSensorInformationChangeListener
import com.improver.app.sensors.interfaces.SensorManager
import com.improver.app.sensors.models.SensorInformations
import kotlinx.android.synthetic.main.fragment_sensor_item.view.*


class SensorRecyclerViewAdapter(
    private val sensorInformations: List<SensorInformations>,
    private val sensorManager: SensorManager?,
    private val context: Context
) : RecyclerView.Adapter<SensorRecyclerViewAdapter.ViewHolder>(){


    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as SensorInformations
            if(item.state != SensorInformations.State.CONNECTED) {
                item.state = SensorInformations.State.CONNECTING
            }
            notifyDataSetChanged()
            sensorManager?.onSensorSelectInteraction(this, item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_sensor_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = sensorInformations[position]
        holder.sensorNameView.text = item.name
        holder.sensorAddressView.text = item.address
        when {
            item.state == SensorInformations.State.DISCONNECTED -> {
                holder.buttonTextView.text = context.getString(R.string.connect)
                holder.buttonView.background = context.getDrawable(R.drawable.shadow_blue_button)
                holder.buttonImageView.setImageDrawable(context.getDrawable(R.drawable.ic_bluetooth))
            }
            item.state == SensorInformations.State.CONNECTING -> {
                holder.buttonTextView.text = context.getString(R.string.connecting)
                holder.buttonView.background = context.getDrawable(R.drawable.shadow_orange)
                holder.buttonImageView.setImageDrawable(context.getDrawable(R.drawable.ic_bluetooth_signal_indicator))
            }
            else -> {
                holder.buttonTextView.text = context.getString(R.string.configure)
                holder.buttonView.background = context.getDrawable(R.drawable.shadow_green)
                holder.buttonImageView.setImageDrawable(context.getDrawable(R.drawable.ic_menu_manage))
            }
        }
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = sensorInformations.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val sensorNameView: TextView = mView.sensorItemName
        val sensorAddressView: TextView = mView.sensorItemAddress
        val buttonTextView: TextView = mView.sensorItemButtonText
        val buttonView: View = mView.sensorItemButton
        val buttonImageView: ImageView = mView.sensorItemButtonImage

        override fun toString(): String {
            return super.toString() + sensorNameView.text.toString() + sensorAddressView.text.toString()
        }
    }
}
