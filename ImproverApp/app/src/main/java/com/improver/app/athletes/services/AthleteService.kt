package com.improver.app.athletes.services

import com.fasterxml.jackson.module.kotlin.readValue
import com.improver.app.athletes.models.Athlete
import com.improver.app.common.services.HttpService
import com.improver.app.common.services.ObjectService
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException


object AthleteService {

    fun getAthletes(callback: (List<Athlete>?) -> Unit, onFailure: ()-> Unit ){
            HttpService.get("/athletes", object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                   onFailure()
                }
                override fun onResponse(call: Call, response: Response) {
                    val string = response.body()?.string()
                    callback(ObjectService.readValue(string!!))
                }
            })
    }

    fun postAthletes(athlete: Athlete, callback: (Athlete) -> Unit, onFailure: ()-> Unit){
        HttpService.post("/athletes", ObjectService.writeValueAsString(athlete), object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                onFailure()
            }
            override fun onResponse(call: Call, response: Response) {
                val string = response.body()?.string()
                callback(ObjectService.readValue(string!!))
            }
        })
    }
}