package com.improver.app.synchronization.persistance

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.OnConflictStrategy



@Dao
interface RequestDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg requests: Request)

    @Delete
    fun delete(request: Request)

    @Query("DELETE FROM request WHERE id <= :id")
    fun deleteRequestsBeforeId(id: Long)

    @Query("SELECT * FROM request ORDER BY id LIMIT 10")
    fun getNext10(): List<Request>

}