package com.improver.app.synchronization.interfaces

interface OnSynchronizationStateChangeListener {
    fun onUnsynchronized()
    fun onSynchronization()
    fun onSynchronized()
}