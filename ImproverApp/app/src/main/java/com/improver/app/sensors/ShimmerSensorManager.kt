package com.improver.app.sensors

import android.app.Activity
import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.improver.app.common.layout.interfaces.FullscreenFragmentActivity
import com.improver.app.common.models.Metric
import com.improver.app.sensors.adapters.SensorRecyclerViewAdapter
import com.improver.app.sensors.fragments.ConfigureSensorFragment
import com.improver.app.sensors.handlers.SensorInformationHolder
import com.improver.app.sensors.interfaces.OnSensorDataListener
import com.improver.app.sensors.interfaces.OnSensorInformationChangeListener
import com.improver.app.sensors.interfaces.SensorManager
import com.improver.app.sensors.models.SensorInformations
import com.shimmerresearch.android.Shimmer
import com.shimmerresearch.android.manager.ShimmerBluetoothManagerAndroid
import com.shimmerresearch.bluetooth.ShimmerBluetooth
import com.shimmerresearch.driver.CallbackObject
import com.shimmerresearch.driver.ObjectCluster
import com.shimmerresearch.driver.ShimmerDevice
import java.lang.RuntimeException
import java.util.*

class ShimmerSensorManager(private val activity: Activity) : SensorManager, Handler() {

    var shimmerBluetoothManager: ShimmerBluetoothManagerAndroid = ShimmerBluetoothManagerAndroid(activity, this)
    override var onSensorInformationChangeListeners: MutableList<OnSensorInformationChangeListener> = LinkedList()
    override var onSensorDataListeners: MutableList<OnSensorDataListener> = LinkedList()
    var stereotypeManager: ShimmerStereotypeManager = ShimmerStereotypeManager()
    var saveOnDisk: Boolean = false

    init {
        shimmerBluetoothManager.disconnectAllDevices()
    }

    override fun disconnectAll() {
        shimmerBluetoothManager.disconnectAllDevices()
    }

    override fun onSensorSelectInteraction(adapter: SensorRecyclerViewAdapter, item: SensorInformations?) {
        if (item != null) {
            if (shimmerBluetoothManager.listOfConnectedDevices.none { it.btConnectionHandle == item.address }) {
                Handler().postDelayed({
                    onSensorInformationChangeListeners.forEach{
                        it.onSensorStateChanged(item.address)
                    }
                }, 30000)
                shimmerBluetoothManager.connectShimmerThroughBTAddress(item.address, activity)
            } else {
                configureSensor(SensorInformationHolder(item, shimmerBluetoothManager))
            }
        }
    }

    private fun configureSensor(sensorInformations: SensorInformationHolder) {
        when (activity is FullscreenFragmentActivity) {
            true -> activity.showFragment(
                ConfigureSensorFragment.newInstance(sensorInformations),
                sensorInformations.sensorInformations.name
            )
            else -> throw RuntimeException("$activity must implement FullscreenFragmentActivity")
        }
    }

    override fun getConnectedSensors(): List<SensorInformations> {
        return shimmerBluetoothManager.listOfConnectedDevices.map {
            SensorInformations(
                activity,
                it.mShimmerUserAssignedName.replace("_", "3-"),
                it.btConnectionHandle,
                SensorInformations.State.CONNECTED
            )
        }
    }


    private fun startStreaming(shimmerDevice: ShimmerDevice) {
        if (!saveOnDisk) {
            stereotypeManager.assignStereotype(shimmerDevice)
        }
        val shimmer: Shimmer = shimmerDevice as Shimmer
        shimmer.enablePCTimeStamps(false)
        shimmer.enableArraysDataStructure(true)
        shimmerBluetoothManager.startStreaming(shimmerDevice.btConnectionHandle)
    }


    override fun startStreaming(saveOnDisk: Boolean) {
        this.saveOnDisk = saveOnDisk
        try {
            shimmerBluetoothManager.listOfConnectedDevices.map {
                startStreaming(it)
            }
        } catch (e: RuntimeException) {
            Toast.makeText(activity, e.message, Toast.LENGTH_LONG).show()
            stopStreaming()
            throw RuntimeException("Unable to start streaming")
        }
    }

    override fun stopStreaming() {
        shimmerBluetoothManager.stopStreamingAllDevices()
        stereotypeManager.clear()
    }

    override fun handleMessage(msg: Message?) {
        when (msg?.what) {
            ShimmerBluetooth.MSG_IDENTIFIER_DATA_PACKET -> if (msg.obj is ObjectCluster) {
                val objc = msg.obj as ObjectCluster
                val sensorNames: Array<String> = objc.sensorDataArray.mSensorNames
                val sensorData: Array<Double> = objc.sensorDataArray.mCalData.toTypedArray()
                val name = when (saveOnDisk) {
                    true -> objc.shimmerName
                    false -> stereotypeManager.getStereotype(objc.macAddress)
                }
                val metric =
                    Metric(name, sensorNames.zip(sensorData).filter { it.first != null }.toMap().toMutableMap())
                onSensorDataListeners.forEach { it.onSensorData(metric) }
            }
            Shimmer.MESSAGE_TOAST -> {
                Toast.makeText(
                    activity,
                    msg.data.getString(Shimmer.TOAST),
                    Toast.LENGTH_SHORT
                ).show()
            }
            ShimmerBluetooth.MSG_IDENTIFIER_STATE_CHANGE -> {
                val objc = msg.obj as ObjectCluster
                var state: ShimmerBluetooth.BT_STATE? = null
                if (msg.obj is ObjectCluster) {
                    state = (msg.obj as ObjectCluster).mState
                } else if (msg.obj is CallbackObject) {
                    state = (msg.obj as CallbackObject).mState
                }
                when (state) {
                    ShimmerBluetooth.BT_STATE.DISCONNECTED -> {
                        onSensorInformationChangeListeners.forEach {
                            it.onSensorStateChanged(objc.macAddress)
                        }
                    }
                    ShimmerBluetooth.BT_STATE.CONNECTION_LOST -> {
                        onSensorInformationChangeListeners.forEach {
                            it.onSensorStateChanged(objc.macAddress)
                        }
                    }
                    ShimmerBluetooth.BT_STATE.CONNECTION_FAILED -> {
                        onSensorInformationChangeListeners.forEach {
                            it.onSensorStateChanged(objc.macAddress)
                        }
                    }
                    ShimmerBluetooth.BT_STATE.CONNECTED -> {
                        onSensorInformationChangeListeners.forEach {
                            it.onSensorStateChanged(objc.macAddress)
                        }
                    }
                    ShimmerBluetooth.BT_STATE.SDLOGGING -> {
                        onSensorInformationChangeListeners.forEach {
                            it.onSensorStateChanged(objc.macAddress)
                        }
                    }
                    else -> {}
                }
            }
        }
    }
}