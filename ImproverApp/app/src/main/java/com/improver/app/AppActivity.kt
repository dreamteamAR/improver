package com.improver.app

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.widget.ImageView
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.widget.TextView
import androidx.room.Room
import com.bumptech.glide.request.RequestOptions
import com.improver.app.athletes.interfaces.AthleteProvider
import com.improver.app.athletes.persistance.AthleteEntity
import com.improver.app.common.layout.interfaces.FullscreenFragmentActivity
import com.improver.app.common.services.ImageService
import com.improver.app.configuration.fragments.SettingsFragment
import com.improver.app.sensors.ShimmerSensorManager
import com.improver.app.sensors.interfaces.SensorManager
import com.improver.app.sensors.interfaces.SensorManagerProvider
import com.improver.app.sessions.fragments.SessionsFragment
import com.improver.app.sessions.interfaces.SessionManager
import com.improver.app.sessions.interfaces.SessionManagerProvider
import com.improver.app.sessions.manager.SynchronizedSessionManager
import java.util.*
import android.speech.tts.TextToSpeech
import com.improver.app.synchronization.BasicSynchronizer
import com.improver.app.synchronization.interfaces.OnSynchronizationStateChangeListener
import com.improver.app.synchronization.interfaces.Synchronizer


class AppActivity : AppCompatActivity(),
    AthleteProvider,
    NavigationView.OnNavigationItemSelectedListener,
    SensorManagerProvider,
    SessionManagerProvider,
    FullscreenFragmentActivity,
    OnSynchronizationStateChangeListener
{
    override fun onUnsynchronized() {
        changeSynchronizationImage(R.drawable.ic_cloud_off_24px)
    }

    override fun onSynchronization() {
        changeSynchronizationImage(R.drawable.ic_cloud_upload_24px)
    }

    override fun onSynchronized() {
        changeSynchronizationImage(R.drawable.ic_cloud_done_24px)
    }

    private val titleStack: Stack<String> = Stack()

    override fun startFullscreen() {
        hideToolbar()
    }

    override fun showFragment(fragment: Fragment, title: String ) {
        changeFragmentPushToBackStack(fragment, title)
    }

    override fun stopFullscreen() {
        showToolbar()
    }

    override fun getSessionManager(): SessionManager? {
        return _sessionManager
    }

    override fun getSensorManager(): SensorManager {
        return _sensorManager!!
    }
    var db: ApplicationDatabase? = null

    private var _sensorManager: SensorManager? = null
    private var _sessionManager: SessionManager? = null
    private var _synchronizer: Synchronizer? = null
    private var _synchronizerThread: Thread? = null
    override var athlete: AthleteEntity? = null
    private val PERMISSIONS_REQUEST_WRITE_STORAGE = 5
    private var textToSpeech: TextToSpeech? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_draw_app)
        db = Room.databaseBuilder(
            applicationContext,
            ApplicationDatabase::class.java, "database-name"
        ).build()


        athlete = intent?.extras!!.get("athlete") as AthleteEntity

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        _sensorManager = ShimmerSensorManager(this)
        _synchronizer = BasicSynchronizer(this, db!!.requestDao())
        _synchronizerThread = Thread(_synchronizer)
        _synchronizerThread?.start()

        val sessionManager = SynchronizedSessionManager(this, intent?.extras!!.get("athlete") as AthleteEntity, _sensorManager!!, _synchronizer!!)
        _sessionManager = sessionManager
        _sensorManager?.onSensorDataListeners?.add(sessionManager)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)
        ImageService.loadInto(navView.getHeaderView(0).findViewById(R.id.athleteImage), athlete!!.imageUrl, RequestOptions.circleCropTransform())
        navView.getHeaderView(0).findViewById<TextView>(R.id.athleteName).text = "${athlete!!.firstName} ${athlete!!.lastName}"
        changeFragmentPushToBackStack(SessionsFragment(), "Sessions")
        if (Build.VERSION.SDK_INT >= 23) {
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    PERMISSIONS_REQUEST_WRITE_STORAGE)
            }
        }

        textToSpeech = TextToSpeech(applicationContext) {
            if(it != TextToSpeech.ERROR) {
                textToSpeech?.language = Locale.UK
            }
        }
    }

    fun changeSynchronizationImage(resourceId: Int){
        runOnUiThread {
            findViewById<ImageView>(R.id.cloudSyncStateImage).setImageDrawable(getDrawable(resourceId))
        }
    }

    fun speekText(text: String) {
        textToSpeech?.speak(text, TextToSpeech.QUEUE_FLUSH, null,null)
    }

    override fun onDestroy() {
        super.onDestroy()
        _synchronizer?.stop()
        _synchronizerThread?.join()
        db!!.close()
        _sensorManager?.disconnectAll()
        _sessionManager?.deinit()
    }

    fun hideToolbar() {
        supportActionBar?.hide()
    }

    fun showToolbar() {
        supportActionBar?.show()
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (supportFragmentManager.backStackEntryCount > 1) {
                if(titleStack.size > 1)
                    titleStack.pop()
                supportActionBar!!.title = titleStack.peek()
                supportFragmentManager.popBackStack()
            } else {
                this.finish()
            }
        }
    }

    private fun changeFragment(fragment: Fragment, title: String?) {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out)
        transaction.replace(R.id.appFragmentContainer, fragment)
        transaction.commit()
        supportActionBar!!.title = title?:supportActionBar!!.title
    }


    fun changeFragmentPushToBackStack(fragment: Fragment, title: String) {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out)
        transaction.replace(R.id.appFragmentContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
        supportActionBar!!.title = title
        titleStack.push(title)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        while(!titleStack.empty()) {
            titleStack.pop()
        }
        while(supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        }
        when (item.itemId) {
            R.id.navigation_sessions -> {
                changeFragmentPushToBackStack(SessionsFragment(), "Sessions")
            }
            R.id.navigation_settings -> {
                changeFragmentPushToBackStack(SettingsFragment(), "Settings")
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

}
