package com.improver.app.sensors.interfaces

interface OnSensorInformationChangeListener {
    fun onSensorStateChanged(bluetoothAddress: String)
}

