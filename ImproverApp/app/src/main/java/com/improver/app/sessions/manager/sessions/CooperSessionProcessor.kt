package com.improver.app.sessions.manager.sessions

import android.content.Context
import android.media.MediaPlayer
import com.improver.app.R
import java.lang.Exception

class CooperSessionProcessor(val context: Context, override val onSessionProcessorEventListener: OnSessionProcessorEventListener): SessionProcessor {

    var preparation = true

    private var currentTime: Long = 0
    private var player: MediaPlayer = MediaPlayer.create(context, R.raw.countdown)
    private var soundPlayed = false
    override fun start() {
        player.setOnCompletionListener {
            player.release()
        }
        preparation = true
        currentTime = System.currentTimeMillis() + 1000 * 30
    }

    override fun update() {
        if(preparation) {
            if(currentTime - System.currentTimeMillis() <= 0)
            {
                preparation = false
                currentTime = System.currentTimeMillis() + 1000 * 60 * 12
                onSessionProcessorEventListener.onSessionStart()
            } else if(currentTime - System.currentTimeMillis() <= 1000*11 && !soundPlayed) {
                soundPlayed = true
                player.start()
            }
        } else {
            if(((getTime()-1000)/1000)%60 == 0L) {
                val minute = getTime()/60000
                if(minute > 1)
                    onSessionProcessorEventListener.communicateEvent("$minute minutes left")
                else if(minute == 1L)
                    onSessionProcessorEventListener.communicateEvent("$minute minute left")
            }
            if(currentTime - System.currentTimeMillis() <= 0) {
                onSessionProcessorEventListener.onSessionEnd()
            }
        }
    }

    override fun getTime(): Long {
        return currentTime - System.currentTimeMillis()
    }

    override fun isReady(): Boolean {
        return !preparation
    }

    override fun stop() {
        try {
            if(player.isPlaying) {
                player.stop()
                player.release()
            }
        } catch( exception: Exception) {

        }
    }

}