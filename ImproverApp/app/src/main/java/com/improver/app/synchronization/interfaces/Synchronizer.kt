package com.improver.app.synchronization.interfaces

import com.improver.app.synchronization.persistance.Request

interface Synchronizer : Runnable {
    fun addRequest(request: Request)
    fun stop()
}