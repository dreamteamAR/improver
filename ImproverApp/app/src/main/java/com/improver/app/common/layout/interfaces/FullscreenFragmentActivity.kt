package com.improver.app.common.layout.interfaces

import androidx.fragment.app.Fragment

interface FullscreenFragmentActivity {
    fun startFullscreen()
    fun showFragment(fragment: Fragment, title: String)
    fun stopFullscreen()
}