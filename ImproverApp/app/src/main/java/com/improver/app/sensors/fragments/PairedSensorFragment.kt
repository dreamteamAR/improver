package com.improver.app.sensors.fragments

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.improver.app.R
import com.improver.app.sensors.models.SensorInformations
import com.improver.app.sensors.adapters.SensorRecyclerViewAdapter
import com.improver.app.sensors.interfaces.OnSensorInformationChangeListener
import com.improver.app.sensors.interfaces.SensorManager
import com.improver.app.sensors.interfaces.SensorManagerProvider
import java.util.*
import kotlin.collections.ArrayList

class PairedSensorFragment : androidx.fragment.app.Fragment(), OnSensorInformationChangeListener {

    private var bluetoothAdapter: BluetoothAdapter? = null

    private var sensorManager: SensorManager? = null

    private var sensorsData: MutableList<SensorInformations> = LinkedList()

    private var recyclerViewAdapter: SensorRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if(bluetoothAdapter == null) {
            Toast.makeText(context, "Bluetooth is not enabled or not available", Toast.LENGTH_SHORT).show()
            sensorsData = ArrayList()
            return
        }
        refreshDevices()
    }

    private fun refreshDevices() {
        val pairedDevices = bluetoothAdapter?.bondedDevices
        val connectedAddresses = sensorManager!!.getConnectedSensors().map{it.address}

        val deviceStates = pairedDevices?.map {
            val state = if(connectedAddresses.none { address -> address == it.address }) {
                SensorInformations.State.DISCONNECTED
            } else {
                SensorInformations.State.CONNECTED
            }
            SensorInformations(
                activity!!,
                it.name,
                it.address,
                state
            )
        }?.sortedBy { it.name }
        sensorsData.clear()
        sensorsData.addAll(deviceStates!!)
        recyclerViewAdapter?.notifyDataSetChanged()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pairedsensor_list, container, false)
        val recycler: androidx.recyclerview.widget.RecyclerView = view.findViewById(R.id.list)
        recyclerViewAdapter = SensorRecyclerViewAdapter(sensorsData, sensorManager, context!!)
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        recycler.adapter = recyclerViewAdapter
        sensorManager?.onSensorInformationChangeListeners?.add(this)
        view.findViewById<TextView>(R.id.disconnect_all_button).setOnClickListener {
            sensorManager?.disconnectAll()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        refreshDevices()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        sensorManager?.onSensorInformationChangeListeners?.remove(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sensorManager = when(context) {
            is SensorManager -> context
            is SensorManagerProvider -> context.getSensorManager()
            else -> throw RuntimeException("$context must implement SensorManager or SensorManagerProvider")
        }
    }

    override fun onDetach() {
        super.onDetach()
        sensorManager = null
    }

    override fun onSensorStateChanged(bluetoothAddress: String) {
        refreshDevice(bluetoothAddress)
    }

    private fun refreshDevice(bluetoothAddress: String) {
        if(sensorManager!!.getConnectedSensors().map { it.address }.filter { it == bluetoothAddress }.isNotEmpty()) {
            sensorsData.filter { it.address == bluetoothAddress && it.state != SensorInformations.State.CONNECTED }.forEach{
                it.state = SensorInformations.State.CONNECTED
                recyclerViewAdapter?.notifyItemChanged(sensorsData.indexOf(it))
            }
        } else {
            sensorsData.filter { it.address == bluetoothAddress && it.state != SensorInformations.State.DISCONNECTED }.forEach{
                it.state = SensorInformations.State.DISCONNECTED
                recyclerViewAdapter?.notifyItemChanged(sensorsData.indexOf(it))
            }
        }
    }
}
