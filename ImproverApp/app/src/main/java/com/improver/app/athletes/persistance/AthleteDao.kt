package com.improver.app.athletes.persistance

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface AthleteDao {
    @Query("SELECT * FROM athleteentity")
    fun findAll(): List<AthleteEntity>

    @Query("SELECT * FROM athleteentity WHERE athleteId=:athleteId LIMIT 1")
    fun findByAthleteId(athleteId: String): AthleteEntity?

    @Update
    fun updateAthlete(athlete: AthleteEntity): Int

    @Insert
    fun insert(athlete: AthleteEntity): Long

    @Delete
    fun delete(athlete: AthleteEntity)

    @Query("DELETE FROM athleteentity")
    fun deleteAll()
}