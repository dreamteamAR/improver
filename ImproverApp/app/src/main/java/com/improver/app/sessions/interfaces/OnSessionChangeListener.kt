package com.improver.app.sessions.interfaces

import com.improver.app.sessions.models.SessionState

interface OnSessionChangeListener {
    fun onSessionStateChange(state: SessionState)
    fun onTimeChange(timestamp: Long)
}