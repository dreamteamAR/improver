package com.improver.app.sensors.interfaces

import com.improver.app.sensors.adapters.SensorRecyclerViewAdapter
import com.improver.app.sensors.models.SensorInformations

interface SensorManager {
    var onSensorInformationChangeListeners: MutableList<OnSensorInformationChangeListener>
    var onSensorDataListeners: MutableList<OnSensorDataListener>
    fun onSensorSelectInteraction(adapter: SensorRecyclerViewAdapter, item: SensorInformations?)
    fun getConnectedSensors(): List<SensorInformations>
    fun startStreaming(saveOnDisk: Boolean)
    fun stopStreaming()
    fun disconnectAll()
}