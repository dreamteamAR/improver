package com.improver.app.sessions.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.improver.app.R
import com.improver.app.common.layout.interfaces.FullscreenFragmentActivity
import com.improver.app.sessions.adapters.SessionTypeAdapter

import com.improver.app.sessions.interfaces.SessionManager
import com.improver.app.sessions.interfaces.SessionManagerProvider

class SessionTypeItemFragment : androidx.fragment.app.Fragment() {

    private var sessionManager: SessionManager? = null
    private var fullscreenFragmentActivity: FullscreenFragmentActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sessiontypeitem_list, container, false)
        val recyclerView: androidx.recyclerview.widget.RecyclerView = view.findViewById(R.id.list)
        with(recyclerView) {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            adapter = SessionTypeAdapter(activity, sessionManager)
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sessionManager = when (context) {
            is SessionManager -> context
            is SessionManagerProvider -> context.getSessionManager()
            else -> throw RuntimeException("$context must implement SessionManager or SessionManagerProvider")
        }
        fullscreenFragmentActivity = when(context is FullscreenFragmentActivity) {
            true -> context
            else -> throw java.lang.RuntimeException("$context must implement FullscreenFragmentActivity")
        }
    }

    override fun onDetach() {
        super.onDetach()
        sessionManager = null
        fullscreenFragmentActivity = null
    }


}
