package com.improver.app.sessions.interfaces

interface SessionManagerProvider {
    fun getSessionManager(): SessionManager?
}