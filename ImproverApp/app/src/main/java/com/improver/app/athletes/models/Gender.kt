package com.improver.app.athletes.models

enum class Gender {
    MALE,
    FEMALE
}