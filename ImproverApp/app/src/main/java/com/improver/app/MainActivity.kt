package com.improver.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.improver.app.athletes.fragments.AthleteFragment
import android.content.Intent
import androidx.room.Room
import com.improver.app.athletes.fragments.NewAthleteFragment
import com.improver.app.athletes.persistance.AthleteEntity

class MainActivity : AppCompatActivity(), AthleteFragment.OnListFragmentInteractionListener {
    override fun onNewAthleteClick() {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.content_main, NewAthleteFragment())
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private var db: ApplicationDatabase? = null


    override fun onListFragmentInteraction(item: AthleteEntity?) {
        val intent = Intent(this, AppActivity::class.java)
        intent.putExtra("athlete", item)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.content_main, AthleteFragment(db!!.athleteDao()))
        transaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = Room.databaseBuilder(
            applicationContext,
            ApplicationDatabase::class.java, "database-name"
        ).fallbackToDestructiveMigration().build()
        setContentView(R.layout.activity_main)

    }

    override fun onDestroy() {
        super.onDestroy()
        db!!.close()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

}
