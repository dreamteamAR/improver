package com.improver.app.common

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView

class Tile(private val tile: View, private var drawableView: ImageView, private val description: TextView) {

    fun setColor(context: Context?, id: Int) {
        tile.background = context!!.getDrawable(id)
    }

    fun setDescription(text: String) {
        description.text = text
    }

}