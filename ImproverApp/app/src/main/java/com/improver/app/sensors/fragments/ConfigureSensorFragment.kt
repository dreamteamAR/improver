package com.improver.app.sensors.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.improver.app.R
import com.improver.app.common.layout.interfaces.FullscreenFragmentActivity
import com.improver.app.sensors.handlers.SensorInformationHolder
import com.shimmerresearch.android.guiUtilities.ShimmerDialogConfigurations
import java.lang.Exception
import java.lang.RuntimeException


class ConfigureSensorFragment : androidx.fragment.app.Fragment() {

    var sensorInformationHolder: SensorInformationHolder? = null
    var fullscreenFragmentActivity: FullscreenFragmentActivity? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_configure_sensor, container, false)
        view.findViewById<View>(R.id.disconnect_button).setOnClickListener {
            sensorInformationHolder?.bluetoothManagerAndroid?.disconnectShimmer(sensorInformationHolder?.sensorInformations?.address)
            activity?.onBackPressed()
        }
        view.findViewById<View>(R.id.enable_sensors_tile).setOnClickListener {
            try {
                ShimmerDialogConfigurations.buildShimmerSensorEnableDetails(
                    sensorInformationHolder!!.bluetoothManagerAndroid.getShimmer(
                        sensorInformationHolder!!.sensorInformations.address
                    ), activity!!, sensorInformationHolder!!.bluetoothManagerAndroid
                )
            } catch (e: Exception) {
                notifyThatDeviceDisconnected()
            }

        }
        view.findViewById<View>(R.id.sensor_settings_tile).setOnClickListener {
            try {
                sensorInformationHolder!!.bluetoothManagerAndroid.getShimmer(sensorInformationHolder!!.sensorInformations.address).shimmerConfigBytes
                ShimmerDialogConfigurations.buildShimmerConfigOptions(
                    sensorInformationHolder!!.bluetoothManagerAndroid.getShimmer(sensorInformationHolder!!.sensorInformations.address),
                    activity!!,
                    sensorInformationHolder!!.bluetoothManagerAndroid
                )
            } catch (e: Exception) {
                notifyThatDeviceDisconnected()
            }
        }
        return view
    }

    private fun notifyThatDeviceDisconnected() {
        Toast.makeText(activity, "Device ${sensorInformationHolder!!.sensorInformations.name} is disconnected or has connection problems.", Toast.LENGTH_SHORT).show()
        activity?.onBackPressed()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fullscreenFragmentActivity = when (context is FullscreenFragmentActivity) {
            true -> context
            else -> throw RuntimeException("$context must implement FullscreenFragmentActivity")
        }
    }

    override fun onDetach() {
        super.onDetach()
        fullscreenFragmentActivity = null
    }

    companion object {
        fun newInstance(sensorInformationHolder: SensorInformationHolder): ConfigureSensorFragment {
            val configureSensorFragment = ConfigureSensorFragment()
            configureSensorFragment.sensorInformationHolder = sensorInformationHolder
            return configureSensorFragment
        }
    }

}
