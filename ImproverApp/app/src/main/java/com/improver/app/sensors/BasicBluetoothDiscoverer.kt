package com.improver.app.sensors

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import com.improver.app.sensors.interfaces.BluetoothDiscoverer
import com.improver.app.sensors.interfaces.OnBluetoothDiscoveryEventListener
import java.util.*

class BasicBluetoothDiscoverer(activity: Activity?) : BluetoothDiscoverer(activity) {


    private var bluetoothAdapter: BluetoothAdapter? = null
    override val onBluetoothDiscoveryEventListeners: MutableList<OnBluetoothDiscoveryEventListener> = LinkedList()

    override fun startDiscovery() {
        if(!isDiscovering()) {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            when(bluetoothAdapter) {
                null -> onBluetoothDiscoveryEventListeners.forEach{it.onDiscoveryFail()}
                else -> {
                    bluetoothAdapter?.startDiscovery()
                    onBluetoothDiscoveryEventListeners.forEach{it.onBluetoothDiscoveryStart()}
                }
            }
        }
    }

    override fun endDiscovery() {
        if(isDiscovering()) {
            bluetoothAdapter?.cancelDiscovery()
            onBluetoothDiscoveryEventListeners.forEach{it.onBluetoothDiscoveryEnd()}
        }
    }

    override fun isDiscovering(): Boolean {
        return when( val it = bluetoothAdapter?.isDiscovering) {
            null -> false
            else -> it
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        when(intent.action) {
            BluetoothDevice.ACTION_FOUND -> {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                if (device.bondState != BluetoothDevice.BOND_BONDED) {
                    onBluetoothDiscoveryEventListeners.forEach { it.onBluetoothDiscoveredDevice(device) }
                }
            }
            BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> onBluetoothDiscoveryEventListeners.forEach { it.onBluetoothDiscoveryEnd() }
        }
    }
}