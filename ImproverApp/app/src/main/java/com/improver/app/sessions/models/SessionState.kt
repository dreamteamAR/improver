package com.improver.app.sessions.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class SessionState: Parcelable {
    CREATING,
    ACTIVE,
    INACTIVE,
    PREPARATION
}