package com.improver.app.athletes.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.improver.app.R


import com.improver.app.athletes.fragments.AthleteFragment.OnListFragmentInteractionListener
import com.improver.app.athletes.persistance.AthleteEntity
import com.improver.app.common.services.ImageService

import kotlinx.android.synthetic.main.fragment_athlete.view.*

class AthleteRecyclerViewAdapter(
    private val mValues: List<AthleteEntity>,
    private val mListener: OnListFragmentInteractionListener?
) : androidx.recyclerview.widget.RecyclerView.Adapter<AthleteRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as AthleteEntity
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_athlete, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mAthleteNameView.text = String.format("%s %s", item.firstName, item.lastName )
        ImageService.loadInto(holder.mAthleteImageView, item.imageUrl)
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(mView) {
        val mAthleteNameView: TextView = mView.sensorItemName
        val mAthleteImageView: ImageView = mView.athleteImage

        override fun toString(): String {
            return super.toString() + " '" + mAthleteNameView.text + "'"
        }
    }
}
