package com.improver.app.common.services

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

object ImageService {

    fun loadInto(view: ImageView, uri: String, options: RequestOptions? = null) {
        if(options != null) {
            Glide.with(view).load(uri).apply(options).into(view)
        } else {
            Glide.with(view).load(uri).into(view)
        }

    }
}