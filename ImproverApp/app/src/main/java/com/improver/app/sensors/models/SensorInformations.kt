package com.improver.app.sensors.models

import android.app.Activity

data class SensorInformations(val activity: Activity, val name: String, val address: String, var state: State) {
    enum class State{
        CONNECTED,
        CONNECTING,
        DISCONNECTED
    }
}