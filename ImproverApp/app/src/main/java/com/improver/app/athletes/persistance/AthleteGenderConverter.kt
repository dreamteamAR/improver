package com.improver.app.athletes.persistance

import androidx.room.TypeConverter
import com.improver.app.athletes.models.Gender

class AthleteGenderConverter {

    @TypeConverter
    fun toGender(s: String): Gender = Gender.valueOf(s)

    @TypeConverter
    fun toString(gender: Gender) = gender.name
}