package com.improver.app.synchronization

import com.improver.app.common.services.HttpService
import com.improver.app.synchronization.interfaces.OnSynchronizationStateChangeListener
import com.improver.app.synchronization.interfaces.Synchronizer
import com.improver.app.synchronization.persistance.Request
import com.improver.app.synchronization.persistance.RequestDao
import okhttp3.Call
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.ConcurrentLinkedQueue
import javax.security.auth.callback.Callback

class BasicSynchronizer(
    private val onSynchronizationStateChangeListener: OnSynchronizationStateChangeListener,
    private val requestDao: RequestDao
): Synchronizer {

    private val queue: ConcurrentLinkedQueue<Request> = ConcurrentLinkedQueue()
    private val saveQueue: ConcurrentLinkedQueue<Request> = ConcurrentLinkedQueue()
    private var running: Boolean = false
    private var sending: Boolean = true
    override fun addRequest(request: Request) {
        saveQueue.add(request)
    }

    override fun run() {
        running = true
        sending = false
        while(running) {
            while(sending){
                Thread.sleep(10)
            }
            when(val request = queue.peek()){
                null -> {
                    queue.addAll(requestDao.getNext10())
                    if(queue.isEmpty()) {
                        onSynchronizationStateChangeListener.onSynchronized()
                        Thread.sleep(1000)
                    }
                }
                else -> {
                    sending = true
                    when(request.method) {
                        "POST" -> postRequest(request)
                        "PUT" -> putRequest(request)
                        else -> { throw IllegalArgumentException("${request.method} not supported in ${this.javaClass}")}
                    }
                }
            }
            while(true) {
                val req = saveQueue.poll()
                if(req == null)
                    break
                else {
                    requestDao.insert(req)
                }
            }
        }
    }

    private fun postRequest(request: Request) {
        HttpService.post(
            request.endpoint,
            request.content,
            object : Callback, okhttp3.Callback {
                override fun onFailure(call: Call, e: IOException) {
                    onSynchronizationStateChangeListener.onUnsynchronized()
                    sending = false
                }

                override fun onResponse(call: Call, response: Response) {
                    onSynchronizationStateChangeListener.onSynchronization()
                    requestDao.delete(request)
                    queue.poll()
                    sending = false
                }

            }
        )
    }

    private fun putRequest(request: Request) {
        HttpService.put(
            request.endpoint,
            request.content,
            object : Callback, okhttp3.Callback {
                override fun onFailure(call: Call, e: IOException) {
                    onSynchronizationStateChangeListener.onUnsynchronized()
                    sending = false
                }

                override fun onResponse(call: Call, response: Response) {
                    onSynchronizationStateChangeListener.onSynchronization()
                    requestDao.delete(request)
                    queue.poll()
                    sending = false
                }

            }
        )
    }


    override fun stop() {
        running = false

    }
}