package com.improver.app.sessions.manager.disksaver

import com.improver.app.common.models.Metric
import java.io.File
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.*


class DiskSaver(val athleteId: String) : Runnable {

    private val APP_FOLDER_NAME = "Data"
    private val APP_DIR_PATH = android.os.Environment.getExternalStorageDirectory().absolutePath + File.separator + APP_FOLDER_NAME + File.separator
    val queue: ConcurrentLinkedQueue<Metric> = ConcurrentLinkedQueue()
    private var running: Boolean = false
    private var files: MutableMap<String, FileHolder> = TreeMap()
    var prefix: String = "NULL"

    override fun run() {
        running = true
        while(running || queue.size > 0) {
            when(val item = queue.peek()) {
                null -> {}
                else -> {
                    if(!files.containsKey(item.metric)) {
                        files[item.metric] = FileHolder(setupFile(item.metric))
                    }
                    files[item.metric]!!.saveData(item.values)
                    queue.poll()
                }
            }
        }
        files.values.forEach {
            it.close()
        }
        files = TreeMap()
    }

    fun stop() {
        running = false
    }

    private fun setupFile(metric: String): File {
        val dir = File(APP_DIR_PATH)
        if (!dir.exists()) {
            dir.mkdir()
        }
        val fileName = prefix + "_" + metric + ".csv"
        val filePath = APP_DIR_PATH + File.separator + fileName
        val file = File(filePath)
        if (!file.exists()) {
            file.createNewFile()
        }
        return file
    }

}