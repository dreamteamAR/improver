package com.improver.app.sensors.interfaces

import android.bluetooth.BluetoothDevice

interface OnBluetoothDiscoveryEventListener  {
    fun onBluetoothDiscoveryStart()
    fun onDiscoveryFail()
    fun onBluetoothDiscoveryEnd()
    fun onBluetoothDiscoveredDevice(device: BluetoothDevice)
}