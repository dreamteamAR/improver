package com.improver.app.athletes.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText

import com.improver.app.R
import android.app.DatePickerDialog
import android.content.res.ColorStateList
import android.widget.Toast
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.core.content.ContextCompat
import com.improver.app.athletes.models.Athlete
import com.improver.app.athletes.models.Gender
import com.improver.app.athletes.services.AthleteService
import java.util.*




class NewAthleteFragment : androidx.fragment.app.Fragment() {

    var picker: DatePickerDialog? = null
    private var firstName: EditText? = null
    private var lastName: EditText? = null
    private var weight: EditText? = null
    private var height: EditText? = null
    private var birthDate: EditText? = null
    private var genderMale: FloatingActionButton? = null
    private var genderFemale: FloatingActionButton? = null
    private var gender: Gender = Gender.MALE
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_new_athlete, container, false)
        val birthDateView: View = view.findViewById(R.id.birthdate)
        birthDate = view.findViewById(R.id.birthdate_text)
        firstName = view.findViewById(R.id.first_name)
        lastName = view.findViewById(R.id.last_name)
        weight = view.findViewById(R.id.weight)
        height = view.findViewById(R.id.height)
        genderMale = view.findViewById(R.id.gender_male)
        genderFemale = view.findViewById(R.id.gender_female)
        genderMale?.setOnClickListener {
            changeGender(Gender.MALE)
        }

        genderFemale?.setOnClickListener {
            changeGender(Gender.FEMALE)
        }

        view.findViewById<View>(R.id.submit).setOnClickListener {
            AthleteService.postAthletes(Athlete(null,
                firstName!!.text.toString(),
                lastName!!.text.toString(),
                "https://icon-library.net/images/avatar-icon/avatar-icon-4.jpg",
                birthDate!!.text.toString(),
                weight!!.text.toString().toInt(),
                height!!.text.toString().toInt(),
                gender
                ),
            {
                activity?.runOnUiThread {
                    activity?.onBackPressed()
                }

            },
                {activity?.runOnUiThread {
                    Toast.makeText(context, "Unable to create athlete. Cannot connect to server.", Toast.LENGTH_SHORT)
                        .show()
                }
                }
            )
        }
        birthDate?.setOnClickListener{
            val cldr = Calendar.getInstance()
            val day = cldr.get(Calendar.DAY_OF_MONTH)
            val month = cldr.get(Calendar.MONTH)
            val year = cldr.get(Calendar.YEAR)
            // date picker dialog
            picker = DatePickerDialog(context!!,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth -> birthDate?.setText(year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth.toString()) },
                year,
                month,
                day
            )
            picker?.show()
        }
        birthDateView.setOnClickListener{
            val cldr = Calendar.getInstance()
            val day = cldr.get(Calendar.DAY_OF_MONTH)
            val month = cldr.get(Calendar.MONTH)
            val year = cldr.get(Calendar.YEAR)
            // date picker dialog
            picker = DatePickerDialog(context!!,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth -> birthDate?.setText(year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth.toString()) },
                year,
                month,
                day
            )
            picker?.show()
        }
        changeGender(Gender.MALE)
        return view
    }

    private fun changeGender(gender: Gender) {
        this.gender = gender
        when(gender) {
            Gender.MALE -> {
                genderMale?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.colorSelected))
                genderFemale?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.colorBlue))
            }
            Gender.FEMALE -> {
                genderMale?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.colorBlue))
                genderFemale?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.colorSelected))
            }
        }
    }

}
