package com.improver.app.sensors.interfaces

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.IntentFilter
import androidx.core.app.ActivityCompat

abstract class BluetoothDiscoverer(val activity: Activity?) : BroadcastReceiver() {
    private val MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 4556
    init {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION
        )
    }
    abstract val onBluetoothDiscoveryEventListeners: MutableList<OnBluetoothDiscoveryEventListener>
    fun register() {
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        activity!!.registerReceiver(this, filter)
    }
    fun unregister() {
        activity!!.unregisterReceiver(this)
    }
    abstract fun startDiscovery()
    abstract fun endDiscovery()
    abstract fun isDiscovering(): Boolean
}