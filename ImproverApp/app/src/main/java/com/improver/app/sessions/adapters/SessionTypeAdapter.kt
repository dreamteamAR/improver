package com.improver.app.sessions.adapters

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.improver.app.R



import com.improver.app.sessions.interfaces.SessionManager
import com.improver.app.sessions.models.SessionType
import kotlinx.android.synthetic.main.fragment_sessiontypeitem.view.*

class SessionTypeAdapter(
    private val activity: Activity?,
    private val sessionManager: SessionManager?
) : RecyclerView.Adapter<SessionTypeAdapter.ViewHolder>() {

    private val mValues: List<String> = SessionType.values().map{ it.toString().toLowerCase().capitalize() }
    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as String
            val type = SessionType.valueOf(item.toUpperCase())
            sessionManager?.sessionType = type
            activity?.onBackPressed()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_sessiontypeitem, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.typeDesc.text = item
        holder.typeName.text = item

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val typeName: TextView = mView.sessionItemName
        val typeDesc: TextView = mView.sessionItemDescription

        override fun toString(): String {
            return super.toString() + " '" + typeName.text + "'"
        }
    }
}
