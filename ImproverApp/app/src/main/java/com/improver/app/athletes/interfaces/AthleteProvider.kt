package com.improver.app.athletes.interfaces

import com.improver.app.athletes.persistance.AthleteEntity

interface AthleteProvider {
    val athlete: AthleteEntity?
}