package com.improver.app.common.services

import okhttp3.Callback
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import java.security.cert.CertificateException
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


object HttpService {

    private const val PORTAL = "https://192.168.0.52:8080/v1"

    // SHOULD BE USED FOR PRODUCTION
    private val client = OkHttpClient.Builder()
        .connectTimeout(5, TimeUnit.SECONDS)
        .writeTimeout(5, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .build()

    // ONLY FOR DEVELOPMENT
    private fun getUnsafeOkHttpClient(): OkHttpClient {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })

            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier { hostname, session -> true }

            return builder
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }


    }

    fun get(url: String, callback: Callback) {
        val request = Request.Builder()
            .url("$PORTAL$url")
            .build()
        getUnsafeOkHttpClient().newCall(request).enqueue(callback)
    }

    fun put(url: String, body: String, callback: Callback) {
        val request = Request.Builder()
            .url("$PORTAL$url")
            .method("PUT", RequestBody.create(MediaType.parse("application/json"), body))
            .build()
        getUnsafeOkHttpClient().newCall(request).enqueue(callback)
    }

    fun post(url: String, body: String, callback: Callback) {
        val request = Request.Builder()
            .url("$PORTAL$url")
            .method("POST", RequestBody.create(MediaType.parse("application/json"), body))
            .build()
        getUnsafeOkHttpClient().newCall(request).enqueue(callback)
    }

    
}