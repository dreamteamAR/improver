package com.improver.app.sessions.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import com.improver.app.R
import com.improver.app.common.layout.interfaces.FullscreenFragmentActivity
import com.improver.app.sessions.interfaces.OnSessionChangeListener
import com.improver.app.sessions.models.SessionState


class TimeFragment(private val onSessionEndClickListener: SessionEndClickListener) : Fragment(), OnSessionChangeListener {

    override fun onSessionStateChange(state: SessionState) {
        sessionInformation?.text = String.format("Session %s", state.toString().toLowerCase())
    }

    override fun onTimeChange(timestamp: Long) {
        val minutes = timestamp / 1000 / 60
        val seconds = timestamp / 1000 % 60
        time?.text = String.format("%d:%d%d", minutes, seconds/10, seconds%10)
    }

    var time: TextView? = null
    var sessionInformation: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_time, container, false)
        time = view.findViewById(R.id.time_view)
        sessionInformation = view.findViewById(R.id.session_information)
        view.findViewById<TextView>(R.id.end_session_button).setOnClickListener{
            activity?.runOnUiThread {

                activity?.onBackPressed()
            }
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        (activity as FullscreenFragmentActivity).startFullscreen()
    }

    override fun onPause() {
        super.onStop()
        (activity as FullscreenFragmentActivity).stopFullscreen()
    }

    override fun onDestroy() {
        super.onDestroy()
        onSessionEndClickListener.onSessionEndClick()
    }

    interface SessionEndClickListener {
        fun onSessionEndClick()
    }
}
