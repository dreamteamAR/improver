package com.improver.app.athletes.models

import android.os.Parcelable
import com.improver.app.common.services.ObjectService
import com.improver.app.sessions.models.Session
import com.improver.app.synchronization.persistance.Request
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Athlete(
    val id: String?,
    val firstName: String,
    val lastName: String,
    val imageUrl: String,
    val birthDate: String,
    val weight: Int,
    val height: Int,
    val gender: Gender) : Parcelable {


    fun createRequest(): Request {
        return Request(null, "POST", "/athletes", ObjectService.writeValueAsString(this))
    }



}

typealias OnCreateSessionResponse = (Session?) -> Unit