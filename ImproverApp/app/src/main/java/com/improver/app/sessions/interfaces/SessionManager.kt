package com.improver.app.sessions.interfaces

import com.improver.app.sessions.models.SessionState
import com.improver.app.sessions.models.SessionType

interface SessionManager {
    var sessionState: SessionState
    var sessionType: SessionType
    var onSessionChangeListeners: MutableList<OnSessionChangeListener>
    fun startSession()
    fun endSession()
    fun getTime(): Long
    fun deinit()
}