package com.improver.portal.raw.entity

import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table
import java.nio.ByteBuffer

@Table("raw_data")
data class RawData(@PrimaryKey val key: RawDataPrimaryKey, val data: ByteBuffer)