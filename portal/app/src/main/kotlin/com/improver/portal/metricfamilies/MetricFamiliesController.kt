package com.improver.portal.metricfamilies

import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.util.StreamUtils
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.io.IOException
import java.nio.charset.Charset

@CrossOrigin
@RestController
class MetricFamiliesController {
    @GetMapping(value = ["/v1/metric-family/{name}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getMetricFamily(@PathVariable name: String): String {
        val resource = ClassPathResource("metric-families/$name.json")
        try {
            val stream = resource.inputStream
            return StreamUtils.copyToString(stream, Charset.forName("UTF-8"))
        } catch (ioe: IOException) {
            throw RuntimeException("Unknown family name $name")
        }
    }
}