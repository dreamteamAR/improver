package com.improver.portal.sessions

import com.improver.portal.sessions.dto.SessionRequestDTO
import com.improver.portal.sessions.entity.SessionMetricFamilies
import com.improver.portal.sessions.repository.SessionMetricFamiliesRepository
import com.improver.portal.sessions.repository.SessionRepository
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin
@RestController
class SessionsController(private val sessionRepository: SessionRepository, private val sessionMetricFamiliesRepository: SessionMetricFamiliesRepository) {

    @GetMapping("/v1/sessions")
    fun findAll() = sessionRepository.findAll()

    @GetMapping("/v1/sessions/{id}")
    fun findById(@PathVariable id: UUID) = sessionRepository.findByKeyId(id)

    @GetMapping("/v1/sessions/{id}/metric-families")
    fun findSessionMetricFamielies(@PathVariable id: UUID) = sessionMetricFamiliesRepository.findById(id)

    @PostMapping("/v1/sessions")
    fun create(@RequestBody dto: SessionRequestDTO) {
        sessionRepository.save(dto.mapToEntity())
        sessionMetricFamiliesRepository.save(SessionMetricFamilies(dto.id, listOf()))
    }

    @PutMapping("/v1/sessions/{id}")
    fun save(@RequestBody dto: SessionRequestDTO, @PathVariable id: UUID) = sessionRepository.save(dto.mapToEntity())

}
