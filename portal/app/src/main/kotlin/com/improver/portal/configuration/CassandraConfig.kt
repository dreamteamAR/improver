package com.improver.portal.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.cassandra.config.AbstractClusterConfiguration
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification
import org.springframework.data.cassandra.core.cql.keyspace.KeyspaceOption
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories


@Configuration
@EnableCassandraRepositories(basePackages = [
    "com.improver.portal.athletes.repository",
    "com.improver.portal.sessions.repository",
    "com.improver.portal.metrics.repository",
    "com.improver.portal.raw.repository"
])
class CassandraConfig(
        @param:Value("\${spring.data.cassandra.keyspace-name}") var keyspace: String,
        @param:Value("\${spring.data.cassandra.contact-points}") var hosts: String
) : AbstractClusterConfiguration() {
    @Bean
    override fun cluster(): CassandraClusterFactoryBean {

        val bean = RetryingCassandraClusterFactoryBean()

        bean.setAddressTranslator(addressTranslator)
        bean.setAuthProvider(authProvider)
        bean.setClusterBuilderConfigurer(clusterBuilderConfigurer)
        bean.setClusterName(clusterName)
        bean.setCompressionType(compressionType)
        bean.setContactPoints(contactPoints)
        bean.setLoadBalancingPolicy(loadBalancingPolicy)
        bean.setMaxSchemaAgreementWaitSeconds(maxSchemaAgreementWaitSeconds)
        bean.setMetricsEnabled(metricsEnabled)
        bean.setNettyOptions(nettyOptions)
        bean.setPoolingOptions(poolingOptions)
        bean.setPort(port)
        bean.setProtocolVersion(protocolVersion)
        bean.setQueryOptions(queryOptions)
        bean.setReconnectionPolicy(reconnectionPolicy)
        bean.setRetryPolicy(retryPolicy)
        bean.setSpeculativeExecutionPolicy(speculativeExecutionPolicy)
        bean.setSocketOptions(socketOptions)
        bean.setTimestampGenerator(timestampGenerator)

        bean.keyspaceCreations = keyspaceCreations
        bean.keyspaceDrops = keyspaceDrops
        bean.startupScripts = startupScripts
        bean.shutdownScripts = shutdownScripts

        return bean
    }

    override fun getKeyspaceCreations(): List<CreateKeyspaceSpecification> {
        val specification = CreateKeyspaceSpecification.createKeyspace(keyspace)
                .ifNotExists()
                .with(KeyspaceOption.DURABLE_WRITES, true)
                .withSimpleReplication()
        return listOf(specification)
    }

    override fun getContactPoints(): String {
        return hosts
    }
}