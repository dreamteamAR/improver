package com.improver.portal.raw.dto

data class RawDataRequestDTO(val metric: String, val headers: List<String>, val values: String)