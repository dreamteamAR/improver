package com.improver.portal.athletes.repository

import com.improver.portal.athletes.entity.Athlete
import org.springframework.data.cassandra.repository.CassandraRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AthleteRepository : CassandraRepository<Athlete, UUID>