package com.improver.portal.metrics.repository

import com.improver.portal.metrics.entity.Metric
import com.improver.portal.metrics.entity.MetricPrimaryKey
import org.springframework.data.cassandra.repository.CassandraRepository
import org.springframework.data.cassandra.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface MetricRepository : CassandraRepository<Metric, MetricPrimaryKey> {
    fun findByKeySessionId(sessionId: UUID): List<Metric>
    @Query("SELECT * FROM metrics WHERE sessionid=:sessionid AND metric=:metric AND timestamp > :start AND timestamp < :end")
    fun findByKeySessionIdAndKeyMetric(@Param("sessionid") sessionId: UUID, @Param("metric") metric: String, @Param("start") start: Long, @Param("end") end: Long): List<Metric>
}