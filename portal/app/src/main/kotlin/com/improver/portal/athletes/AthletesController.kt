package com.improver.portal.athletes

import com.improver.portal.athletes.entity.Athlete
import com.improver.portal.athletes.repository.AthleteRepository
import com.improver.portal.sessions.repository.SessionRepository
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin
@RestController
class AthletesController(private val athleteRepository: AthleteRepository, private val sessionRepository: SessionRepository) {

    @GetMapping("/v1/athletes")
    fun findAll() = athleteRepository.findAll()

    @GetMapping("/v1/athletes/{id}")
    fun findAthleteById(@PathVariable id: UUID) = athleteRepository.findById(id)

    @GetMapping("/v1/athletes/{id}/sessions")
    fun findAllAthleteSessions(@PathVariable id: UUID) = sessionRepository.findByKeyAthleteId(id)

    @PostMapping(path = ["/v1/athletes"], consumes = ["application/json"], produces = ["application/json"])
    fun save(@RequestBody athlete: Athlete): Athlete {
        if (athlete.id == null) {
            athlete.id = UUID.randomUUID()
        }

        return athleteRepository.save(athlete)
    }

}