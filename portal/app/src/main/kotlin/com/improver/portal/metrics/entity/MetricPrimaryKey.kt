package com.improver.portal.metrics.entity

import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn
import java.util.*

@PrimaryKeyClass
class MetricPrimaryKey(
        @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.PARTITIONED)
        var sessionId: UUID,
        @PrimaryKeyColumn(ordinal = 1, type = PrimaryKeyType.CLUSTERED)
        var metric: String,
        @PrimaryKeyColumn(ordinal = 2, type = PrimaryKeyType.CLUSTERED)
        var timestamp: Long
)
