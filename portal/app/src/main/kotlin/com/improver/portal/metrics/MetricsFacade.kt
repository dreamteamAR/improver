package com.improver.portal.metrics

import com.improver.portal.metrics.repository.MetricRepository
import com.improver.portal.metrics.transfer.MetricDTO
import org.springframework.stereotype.Component
import java.util.*

@Component
class MetricsFacade(private val metricsRepository: MetricRepository) {
    fun findMetrics(sessionId: UUID, metric: String, start: Long, end: Long, cycle: Long): List<MetricDTO> {
        val metrics = metricsRepository.findByKeySessionIdAndKeyMetric(sessionId, metric, start, end)
        metrics.forEach { it.key.timestamp = (it.key.timestamp / cycle) * cycle }
        return metrics.groupBy { it.key.timestamp }
                .map {
                    MetricDTO(
                            it.value.minBy { metric -> metric.value }!!.value,
                            it.value.sumByDouble { metric -> metric.value } / it.value.size,
                            it.value.maxBy { metric -> metric.value }!!.value,
                            it.key
                    )
                }
    }
}