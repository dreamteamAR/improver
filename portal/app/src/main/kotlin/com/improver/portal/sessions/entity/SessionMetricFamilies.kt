package com.improver.portal.sessions.entity

import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table
import java.util.*

@Table("sessions_metric_families")
class SessionMetricFamilies(
        @PrimaryKey
        var sessionId: UUID,
        var metricFamilies: List<String> = listOf()
)