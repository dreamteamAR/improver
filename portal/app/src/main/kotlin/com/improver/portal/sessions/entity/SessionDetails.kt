package com.improver.portal.sessions.entity

import com.datastax.driver.core.DataType
import org.springframework.data.cassandra.core.mapping.CassandraType
import org.springframework.data.cassandra.core.mapping.UserDefinedType

@UserDefinedType
class SessionDetails(
        @CassandraType(type = DataType.Name.VARCHAR)
        val type: SessionType,
        var distance: Long?
)
