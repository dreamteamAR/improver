package com.improver.portal.sessions.entity

import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table

@Table("sessions")
data class Session(
        @PrimaryKey
        var key: SessionPrimaryKey,
        var startTime: Long,
        var endTime: Long,
        var details: SessionDetails
)
