package com.improver.portal.metrics.entity

import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table

@Table("metrics")
class Metric(
        @PrimaryKey
        var key: MetricPrimaryKey,
        var value: Double
)