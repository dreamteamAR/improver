package com.improver.portal.athletes.entity

import com.datastax.driver.core.DataType
import org.springframework.data.cassandra.core.mapping.CassandraType
import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table
import java.util.*

@Table("athletes")
data class Athlete(
        @PrimaryKey
        var id: UUID?,
        var firstName: String,
        var lastName: String,
        var imageUrl: String,
        var weight: Int,
        var height: Int,
        var birthDate: String,
        @CassandraType(type = DataType.Name.VARCHAR)
        var gender: Gender
)