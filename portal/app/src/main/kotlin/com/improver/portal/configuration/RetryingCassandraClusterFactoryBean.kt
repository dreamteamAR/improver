package com.improver.portal.configuration

import com.datastax.driver.core.exceptions.NoHostAvailableException
import com.datastax.driver.core.exceptions.TransportException
import org.slf4j.LoggerFactory
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean


class RetryingCassandraClusterFactoryBean : CassandraClusterFactoryBean() {

    @Throws(Exception::class)
    override fun afterPropertiesSet() {
        connect()
    }

    @Throws(Exception::class)
    private fun connect() {
        try {
            super.afterPropertiesSet()
        } catch (e: TransportException) {
            LOG.warn(e.message)
            LOG.warn("Retrying connection in 10 seconds")
            sleep()
            connect()
        } catch (e: IllegalArgumentException) {
            LOG.warn(e.message)
            LOG.warn("Retrying connection in 10 seconds")
            sleep()
            connect()
        } catch (e: NoHostAvailableException) {
            LOG.warn(e.message)
            LOG.warn("Retrying connection in 10 seconds")
            sleep()
            connect()
        }

    }

    private fun sleep() {
        try {
            Thread.sleep(10000)
        } catch (ignored: InterruptedException) {
        }

    }

    companion object {

        private val LOG = LoggerFactory.getLogger(RetryingCassandraClusterFactoryBean::class.java)
    }
}