package com.improver.portal.sessions.entity

import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn
import java.util.*

@PrimaryKeyClass
class SessionPrimaryKey(
        @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.CLUSTERED) var id: UUID?,
        @PrimaryKeyColumn(ordinal = 1, type = PrimaryKeyType.PARTITIONED) var athleteId: UUID
)
