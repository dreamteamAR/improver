package com.improver.portal.sessions.dto

import com.improver.portal.sessions.entity.Session
import com.improver.portal.sessions.entity.SessionDetails
import com.improver.portal.sessions.entity.SessionPrimaryKey
import java.util.*

data class SessionRequestDTO(var id: UUID, var athleteId: UUID, var startTime: Long, var details: SessionDetails, var endTime: Long) {
    fun mapToEntity(): Session {
        return Session(SessionPrimaryKey(id, athleteId), startTime, endTime, details)
    }
}