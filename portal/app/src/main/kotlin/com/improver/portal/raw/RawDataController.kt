package com.improver.portal.raw

import com.fasterxml.jackson.databind.ObjectMapper
import com.improver.portal.raw.dto.RawDataRequestDTO
import com.improver.portal.raw.repository.RawDataRepository
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin
@RestController
class RawDataController(private val rawDataRepository: RawDataRepository, private val kafkaTemplate: KafkaTemplate<String, String>) {

    @PostMapping("/v1/sessions/{sessionId}/raw-data")
    fun sendForProcessing(@PathVariable sessionId: UUID, @RequestBody payload: RawDataRequestDTO) {
        val objectMapper = ObjectMapper()
        kafkaTemplate.send("worker", "{\"sessionId\":\"$sessionId\", \"data\": ${objectMapper.writeValueAsString(payload)}}")
    }
}