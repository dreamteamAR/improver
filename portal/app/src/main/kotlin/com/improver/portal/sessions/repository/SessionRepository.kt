package com.improver.portal.sessions.repository

import com.improver.portal.sessions.entity.Session
import com.improver.portal.sessions.entity.SessionPrimaryKey
import org.springframework.data.cassandra.repository.CassandraRepository
import java.util.*

interface SessionRepository : CassandraRepository<Session, SessionPrimaryKey> {
    fun findByKeyAthleteId(id: UUID): MutableList<Session>
    fun findByKeyId(id: UUID): MutableList<Session>
}
