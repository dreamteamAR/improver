package com.improver.portal.sessions.repository

import com.improver.portal.sessions.entity.SessionMetricFamilies
import org.springframework.data.cassandra.repository.CassandraRepository
import java.util.*

interface SessionMetricFamiliesRepository : CassandraRepository<SessionMetricFamilies, UUID>