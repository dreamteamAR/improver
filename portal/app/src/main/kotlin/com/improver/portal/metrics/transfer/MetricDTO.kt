package com.improver.portal.metrics.transfer

data class MetricDTO(val min: Double, val avg: Double, val max: Double, val timestamp: Long)