package com.improver.portal.metrics

import com.improver.portal.metrics.transfer.MetricDTO
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin
@RestController
class MetricsController(private val metricsFacade: MetricsFacade) {
    @GetMapping("/v1/sessions/{sessionId}/metrics/{metric}")
    fun findBySessionIdAndMetric(
            @PathVariable sessionId: UUID,
            @PathVariable metric: String,
            @RequestParam(defaultValue = "0") start: Long,
            @RequestParam(defaultValue = Long.MAX_VALUE.toString()) end: Long,
            @RequestParam(defaultValue = "1000") cycle: Long): List<MetricDTO> {
        return metricsFacade.findMetrics(sessionId, metric, start, end, cycle)
    }
}