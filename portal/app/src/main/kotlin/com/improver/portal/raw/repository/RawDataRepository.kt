package com.improver.portal.raw.repository

import com.improver.portal.raw.entity.RawData
import com.improver.portal.raw.entity.RawDataPrimaryKey
import org.springframework.data.cassandra.repository.CassandraRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RawDataRepository : CassandraRepository<RawData, RawDataPrimaryKey> {
    fun findByKeySessionId(sessionId: UUID): List<RawData>
}