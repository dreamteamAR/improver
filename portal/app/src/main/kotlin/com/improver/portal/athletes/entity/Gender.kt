package com.improver.portal.athletes.entity

enum class Gender {
    MALE,
    FEMALE
}