package com.improver.portal.raw.entity

import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn
import java.util.*

@PrimaryKeyClass
class RawDataPrimaryKey(
        @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.PARTITIONED)
        val sessionId: UUID,
        @PrimaryKeyColumn(ordinal = 1, type = PrimaryKeyType.CLUSTERED)
        val source: String,
        @PrimaryKeyColumn(ordinal = 2, type = PrimaryKeyType.CLUSTERED)
        val number: Long
)