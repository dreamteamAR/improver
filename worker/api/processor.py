from abc import ABC
from collections import namedtuple
from typing import Dict, List
from uuid import UUID

Result = namedtuple('Result', ['metric', 'timestamp', 'value'])


class Metric:

    def __init__(self, session_id: UUID, metric: str, timestamp: int, value: float):
        self.session_id = session_id
        self.metric = metric
        self.timestamp = int(timestamp)
        self.value = value

    @classmethod
    def new(cls, session_id: UUID, result: Result):
        return cls(session_id, result.metric, result.timestamp, result.value)


class ImproverProcessor(ABC):

    def __init__(self):
        self.__results = []

    def clean_results(self):
        self.__results = []

    def get_results(self) -> List[Result]:
        return self.__results

    def add_result(self, metric: str, timestamp: float, value: float):
        self.__results.append((Result(metric, timestamp, value)))

    def on_data(self, data: dict):
        """
        Is called when data is read with tag on which Processor is registered as listener
        :param data: data from sensors
        """
        raise NotImplementedError()

    def get_state(self) -> dict:
        """
        Is called when leaving processed session
        :returns dict with state
        """
        raise NotImplementedError()

    def restore_state(self, state: dict):
        """
        Is called when there is session to restore
        :param state: saved state or none
        """
        raise NotImplementedError()
