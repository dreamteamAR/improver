from uuid import UUID

from aiocassandra import aiosession
from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.query import BatchStatement

from typing import List

from api.processor import Metric


class DbRawData:

    def __init__(self, session_id: UUID, source: str, number: int, data: bytes):
        self.session_id = session_id
        self.source = source
        self.number = number
        self.data = data


class CassandraHandler:

    def __init__(self, contact_points: List[str]):
        self.cluster = Cluster(contact_points)
        self.session = self.cluster.connect('test_improver')
        aiosession(self.session)

    async def insert_metrics(self, metrics: List[Metric]):
        if len(metrics) == 0:
            return
        query = await self.session.prepare_future('INSERT INTO metrics (sessionid, metric, timestamp, value) VALUES (?, ?, ?, ?)')
        batch = BatchStatement(consistency_level=ConsistencyLevel.QUORUM)
        for metric in metrics:
            batch.add(query, (metric.session_id, metric.metric, metric.timestamp, metric.value))

        await self.session.execute_future(batch)

    async def get_session_metric_families(self, session: UUID) -> List[str]:
        query = await self.session.prepare_future('SELECT * FROM sessions_metric_families WHERE sessionid=?')
        result = await self.session.execute_future(query, (session, ))
        if result[0].metricfamilies is not None:
            return result[0].metricfamilies
        return []

    async def update_session_metric_families(self, session: UUID, metric_families: List[str]) -> List[str]:
        query = await self.session.prepare_future('UPDATE sessions_metric_families SET metricfamilies=? WHERE sessionid=?')
        return await self.session.execute_future(query, (metric_families, session))

    async def insert_raw_data(self, raw_data: DbRawData):
        query = await self.session.prepare_future('INSERT INTO raw_data (sessionid, source, number, data) VALUES (?, ?, ?, ?)')
        await self.session.execute_future(query, (raw_data.session_id, raw_data.source, raw_data.number, raw_data.data))
