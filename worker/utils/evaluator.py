from argparse import ArgumentParser
import pandas as pd
import os
import numpy as np
import peakutils

from BioSPPy.biosppy.signals import ecg
from BioSPPy.biosppy.signals import resp


class Evaluator:
    def __init__(self):
        self.__test_duration = 12.0
        self.__dataset_path = "."
        self.__file_prefix = "rrrr-mm-dd_hh-mm-ss"
        self.__parser = None
        self.__ecg_sampling_rate = 256.0
        self.__resp_sampling_rate = 204.8
        self.__imu_sampling_rate = 256.0
        self.__gsr_sampling_rate = 256.0
        self.__ecg_file = ""
        self.__resp_file = ""
        self.__imu_file = ""
        self.__gsr_file = ""
        self.__ecg_data = {}
        self.__resp_data = {}
        self.__imu_data = {}
        self.__gsr_data = {}
        self.__athlete_gender = None
        self.__athlete_age = None
        self.__athlete_weight = None
        self.__athlete_height = None
        self.__distance = 0
        self.__record = ""

    def init_parser(self):
        self.__parser = ArgumentParser()
        self.__parser.add_argument("-path", dest="dataset_path", default=self.__dataset_path, type=str,
                                   help="path to directory with csv files")
        self.__parser.add_argument("-prefix", dest="file_prefix", default=self.__file_prefix, type=str,
                                   help="data files prefix")
        self.__parser.add_argument("-gender", dest="athlete_gender", default="m", type=str, choices=['m', 'f'],
                                   help="athlete's gender, possible values: 'm' for male or 'f' for female")
        self.__parser.add_argument("-age", dest="athlete_age", default=None, type=int, choices=range(13, 100),
                                   help="athlete's age")
        self.__parser.add_argument("-weight", dest="athlete_weight", default=None, type=int, choices=range(20, 200),
                                   help="athlete's weight in kg")
        self.__parser.add_argument("-height", dest="athlete_height", default=None, type=int, choices=range(60, 250),
                                   help="athlete's height in cm")
        self.__parser.add_argument("-distance", dest="distance", default=None, type=int,
                                   help="distance run during test in meters")

    def parse(self):
        args = self.__parser.parse_args()
        self.__dataset_path = args.dataset_path
        self.__file_prefix = args.file_prefix
        self.__athlete_gender = args.athlete_gender
        self.__athlete_height = int(args.athlete_height)
        self.__athlete_weight = int(args.athlete_weight)
        self.__athlete_age = int(args.athlete_age)
        self.__distance = int(args.distance)

    def find_files(self):
        try:
            self.__ecg_file = os.path.join(self.__dataset_path, f"{self.__file_prefix}_ecg.csv")
            self.__resp_file = os.path.join(self.__dataset_path, f"{self.__file_prefix}_resp.csv")
            self.__imu_file = os.path.join(self.__dataset_path, f"{self.__file_prefix}_imu.csv")
            self.__gsr_file = os.path.join(self.__dataset_path, f"{self.__file_prefix}_gsr.csv")
        except FileNotFoundError as e:
            raise Exception(e.filename)

    def fill_data(self):
        ecg = pd.read_csv(self.__ecg_file)
        resp = pd.read_csv(self.__resp_file)
        imu = pd.read_csv(self.__imu_file)
        gsr = pd.read_csv(self.__gsr_file)
        ecg.head()
        resp.head()
        imu.head()
        gsr.head()
        self.__ecg_data = {"values": ecg['ECG_Vx-RL_24BIT'], "timestamps": ecg['Timestamp']}
        self.__resp_data = {"values": resp['ECG_RESP_24BIT'], "timestamps": resp['Timestamp']}
        self.__imu_data = {"values": imu['Gyro_Z'], "timestamps": imu['Timestamp']}
        self.__gsr_data = {"values": gsr['GSR_Skin_Resistance'], "timestamps": gsr['Timestamp']}

    def evaluate_gender(self):
        self.__record += f"{str(self.__athlete_gender)};"

    def evaluate_age(self):
        self.__record += f"{self.__athlete_age};"

    def evaluate_height(self):
        self.__record += f"{self.__athlete_height};"

    def evaluate_weight(self):
        self.__record += f"{self.__athlete_weight};"

    def evaluate_bmi(self):
        bmi = float(self.__athlete_weight) / ((self.__athlete_height * 0.01) ** 2)
        self.__record += f"{bmi};"

    def evaluate_hr_percentile_50(self):
        processed_values = ecg.ecg(signal=self.__ecg_data['values'], sampling_rate=self.__ecg_sampling_rate, show=False)
        heart_rate = processed_values['heart_rate']
        percentile_50 = np.percentile(heart_rate, 50)
        self.__record += f"{percentile_50};"

    def evaluate_hr_percentile_90(self):
        processed_values = ecg.ecg(signal=self.__ecg_data['values'], sampling_rate=self.__ecg_sampling_rate, show=False)
        heart_rate = processed_values['heart_rate']
        percentile_90 = np.percentile(heart_rate, 90)
        self.__record += f"{percentile_90};"

    def evaluate_resp_rate_percentile_50(self):
        processed_values = resp.resp(signal=self.__resp_data['values'], sampling_rate=self.__resp_sampling_rate, show=False)
        resp_rate = processed_values['resp_rate']
        percentile_50 = np.percentile(resp_rate, 50)
        self.__record += f"{percentile_50};"

    def evaluate_resp_rate_percentile_90(self):
        processed_values = resp.resp(signal=self.__resp_data['values'], sampling_rate=self.__resp_sampling_rate, show=False)
        resp_rate = processed_values['resp_rate']
        percentile_90 = np.percentile(resp_rate, 90)
        self.__record += f"{percentile_90};"

    def evaluate_steps_per_minute(self):
        tresholded = [0 if x < 80 else x for x in self.__imu_data['values']]
        max_value = max(tresholded)
        steps_indices = peakutils.indexes(np.array(tresholded), thres=0.01 / max_value,
                                          min_dist=int(self.__imu_sampling_rate / 2.0))
        detected_steps = len(steps_indices) * 2
        steps_per_min = detected_steps / self.__test_duration * 60
        self.__record += f"{steps_per_min};"

    def evaluate_total_steps(self):
        tresholded = [0 if x < 80 else x for x in self.__imu_data['values']]
        max_value = max(tresholded)
        steps_indices = peakutils.indexes(np.array(tresholded), thres=0.01 / max_value,
                                          min_dist=int(self.__imu_sampling_rate / 2.0))
        detected_steps = len(steps_indices) * 2
        self.__record += f"{detected_steps};"

    def evaluate_distance(self):
        self.__record += f"{self.__distance};"

    def evaluate_label_normal(self):
        label = None
        if 13 <= self.__athlete_age <= 14:
            if self.__athlete_gender == 'm':
                if self.__distance > 2700:
                    label = "very_good"
                elif 2400 <= self.__distance <= 2700:
                    label = "good"
                elif 2200 <= self.__distance <= 2399:
                    label = "average"
                elif 2100 <= self.__distance <= 2199:
                    label = "bad"
                elif self.__distance < 2100:
                    label = "very_bad"
            else:
                if self.__distance > 2000:
                    label = "very_good"
                elif 1900 <= self.__distance <= 2000:
                    label = "good"
                elif 1600 <= self.__distance <= 1899:
                    label = "average"
                elif 1500 <= self.__distance <= 1599:
                    label = "bad"
                elif self.__distance < 1500:
                    label = "very_bad"
        elif 15 <= self.__athlete_age <= 16:
            if self.__athlete_gender == 'm':
                if self.__distance > 2800:
                    label = "very_good"
                elif 2500 <= self.__distance <= 2900:
                    label = "good"
                elif 2300 <= self.__distance <= 2499:
                    label = "average"
                elif 2200 <= self.__distance <= 2299:
                    label = "bad"
                elif self.__distance < 2200:
                    label = "very_bad"
            else:
                if self.__distance > 2100:
                    label = "very_good"
                elif 2000 <= self.__distance <= 2100:
                    label = "good"
                elif 1700 <= self.__distance <= 1999:
                    label = "average"
                elif 1600 <= self.__distance <= 1699:
                    label = "bad"
                elif self.__distance < 1600:
                    label = "very_bad"
        elif 17 <= self.__athlete_age <= 19:
            if self.__athlete_gender == 'm':
                if self.__distance > 3000:
                    label = "very_good"
                elif 2700 <= self.__distance <= 3000:
                    label = "good"
                elif 2500 <= self.__distance <= 2699:
                    label = "average"
                elif 2300 <= self.__distance <= 2499:
                    label = "bad"
                elif self.__distance < 2300:
                    label = "very_bad"
            else:
                if self.__distance > 2300:
                    label = "very_good"
                elif 2100 <= self.__distance <= 2300:
                    label = "good"
                elif 1800 <= self.__distance <= 2099:
                    label = "average"
                elif 1700 <= self.__distance <= 1799:
                    label = "bad"
                elif self.__distance < 1700:
                    label = "very_bad"
        elif 20 <= self.__athlete_age <= 29:
            if self.__athlete_gender == 'm':
                if self.__distance > 2800:
                    label = "very_good"
                elif 2400 <= self.__distance <= 2800:
                    label = "good"
                elif 2200 <= self.__distance <= 2399:
                    label = "average"
                elif 1600 <= self.__distance <= 2199:
                    label = "bad"
                elif self.__distance < 1600:
                    label = "very_bad"
            else:
                if self.__distance > 2700:
                    label = "very_good"
                elif 2200 <= self.__distance <= 2700:
                    label = "good"
                elif 1800 <= self.__distance <= 2199:
                    label = "average"
                elif 1500 <= self.__distance <= 1799:
                    label = "bad"
                elif self.__distance < 1500:
                    label = "very_bad"
        elif 30 <= self.__athlete_age <= 39:
            if self.__athlete_gender == 'm':
                if self.__distance > 2700:
                    label = "very_good"
                elif 2300 <= self.__distance <= 2700:
                    label = "good"
                elif 1900 <= self.__distance <= 2299:
                    label = "average"
                elif 1500 <= self.__distance <= 1899:
                    label = "bad"
                elif self.__distance < 1500:
                    label = "very_bad"
            else:
                if self.__distance > 2500:
                    label = "very_good"
                elif 2000 <= self.__distance <= 2500:
                    label = "good"
                elif 1700 <= self.__distance <= 1999:
                    label = "average"
                elif 1400 <= self.__distance <= 1699:
                    label = "bad"
                elif self.__distance < 1400:
                    label = "very_bad"
        elif 40 <= self.__athlete_age <= 49:
            if self.__athlete_gender == 'm':
                if self.__distance > 2500:
                    label = "very_good"
                elif 2100 <= self.__distance <= 2500:
                    label = "good"
                elif 1700 <= self.__distance <= 2099:
                    label = "average"
                elif 1400 <= self.__distance <= 1699:
                    label = "bad"
                elif self.__distance < 1400:
                    label = "very_bad"
            else:
                if self.__distance > 2300:
                    label = "very_good"
                elif 1900 <= self.__distance <= 2300:
                    label = "good"
                elif 1500 <= self.__distance <= 1899:
                    label = "average"
                elif 1200 <= self.__distance <= 1499:
                    label = "bad"
                elif self.__distance < 1200:
                    label = "very_bad"
        elif self.__athlete_age >= 50:
            if self.__athlete_gender == 'm':
                if self.__distance > 2400:
                    label = "very_good"
                elif 2000 <= self.__distance <= 2400:
                    label = "good"
                elif 1600 <= self.__distance <= 1999:
                    label = "average"
                elif 1300 <= self.__distance <= 1599:
                    label = "bad"
                elif self.__distance < 1300:
                    label = "very_bad"
            else:
                if self.__distance > 2200:
                    label = "very_good"
                elif 1700 <= self.__distance <= 2200:
                    label = "good"
                elif 1400 <= self.__distance <= 1699:
                    label = "average"
                elif 1100 <= self.__distance <= 1399:
                    label = "bad"
                elif self.__distance < 1100:
                    label = "very_bad"
        else:
            raise Exception("Data out of range")
        self.__record += f"{label}"

    def write_to_file(self, filename):
        with open(filename, 'a') as dataset_file:
            dataset_file.write(f"{self.__record}\n")


    def run(self):
        self.init_parser()
        self.parse()
        self.find_files()
        self.fill_data()
        self.evaluate_gender()
        self.evaluate_age()
        self.evaluate_height()
        self.evaluate_weight()
        self.evaluate_bmi()
        self.evaluate_hr_percentile_50()
        self.evaluate_hr_percentile_90()
        self.evaluate_resp_rate_percentile_50()
        self.evaluate_resp_rate_percentile_90()
        self.evaluate_steps_per_minute()
        self.evaluate_total_steps()
        self.evaluate_distance()
        self.evaluate_label_normal()
        self.write_to_file('./dataset/dataset.csv')


if __name__ == '__main__':
    evaluator = Evaluator()
    evaluator.run()
















