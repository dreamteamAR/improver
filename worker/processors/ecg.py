from typing import Optional
from api.processor import ImproverProcessor
from BioSPPy.biosppy.signals import ecg


class Processor(ImproverProcessor):
    def __init__(self, sampling_rate=256, processing_window_sec=15.0, window_translation=5.0):
        """
        :param sampling_rate: Sampling rate in Hz
        :param processing_window_sec: Processing window time in seconds
        """
        super().__init__()
        self.__window_translation = window_translation
        self.__raw_ecg_values = []
        self.__timestamps = []
        self.__sampling_rate = sampling_rate
        self.__processing_window_sec = processing_window_sec
        self.__start_time = None

    def on_data(self, data: dict):
        self.__raw_ecg_values.append(data['ECG_LL-RA_24BIT'])
        self.__timestamps.append(data['Timestamp'])
        if self.__start_time is None:
            self.__start_time = data['System_Timestamp'] - data['Timestamp']
        if self.__ready_for_processing():
            self.__process()
            self.__move_window()

    def get_state(self) -> dict:
        return {
            'start_time': self.__start_time,
            'values_buffer': self.__raw_ecg_values,
            'timestamps_buffer': self.__timestamps
        }

    def restore_state(self, state: Optional[dict]):
        if state is None:
            return
        self.__start_time = state['start_time']
        self.__raw_ecg_values = state['values_buffer']
        self.__timestamps = state['timestamps_buffer']

    def __process(self):
        processed_values = ecg.ecg(signal=self.__raw_ecg_values, sampling_rate=self.__sampling_rate, show=False)
        rpeaks = processed_values['rpeaks']
        if len(rpeaks) > 0:
            avg_hr = float(len(rpeaks))/((self.__timestamps[-1]-self.__timestamps[0])/1000.0)*60.0
            self.add_result('bpm', self.__start_time + self.__timestamps[-1], avg_hr)
        timediff_prev = None
        square_sum = 0
        for j in range(1, len(rpeaks)):
            timediff = self.__timestamps[rpeaks[j]] - self.__timestamps[rpeaks[j - 1]]
            if timediff_prev is None:
                timediff_prev = timediff
                continue
            delta = abs(timediff - timediff_prev)
            timediff_prev = timediff
            square_sum += delta**2
        hrv = (square_sum/len(rpeaks) - 2)**0.5
        self.add_result('hrv', self.__start_time+self.__timestamps[-1], hrv)

    def __move_window(self):
        last = self.__timestamps[len(self.__timestamps)-1]
        while self.__timestamps[0] < last - (self.__processing_window_sec - self.__window_translation) * 1000:
            del self.__timestamps[0]
            del self.__raw_ecg_values[0]

    def __reset(self):
        self.__raw_ecg_values = []
        self.__timestamps = []
        self.__start_time = None

    def __ready_for_processing(self) -> bool:
        return self.__timestamps[-1] - self.__timestamps[0] >= self.__processing_window_sec*1000
