from typing import Optional

import peakutils
import numpy as np

from api.processor import ImproverProcessor


class Processor(ImproverProcessor):
    def __init__(self, sampling_rate=256, processing_window_sec=5.0):
        """
        :param sampling_rate: Sampling rate in Hz
        :param processing_window_sec: Processing window time in seconds
        """
        super().__init__()
        self.__raw_gyro_values = []
        self.__timestamps = []
        self.__sampling_rate = sampling_rate
        self.__processing_window_sec = processing_window_sec
        self.__start_time = None
        self.__steps_total = 0

    def on_data(self, data: dict):
        self.__raw_gyro_values.append(data['Gyro_Z'])
        self.__timestamps.append(data['Timestamp'])
        if self.__start_time is None:
            self.__start_time = data['System_Timestamp'] - data['Timestamp']
        if self.__ready_for_processing():
            self.__process()
            self.__reset()

    def get_state(self) -> dict:
        return {
            'start_time': self.__start_time,
            'values_buffer': self.__raw_gyro_values,
            'timestamps_buffer': self.__timestamps,
            'steps_total': self.__steps_total
        }

    def restore_state(self, state: Optional[dict]):
        if state is None:
            return
        self.__start_time = state['start_time']
        self.__raw_gyro_values = state['values_buffer']
        self.__timestamps = state['timestamps_buffer']
        self.__steps_total = state['steps_total']

    def __process(self):
        buffer_tresholded = [0 if x < 80 else x for x in self.__raw_gyro_values]
        max_value = max(buffer_tresholded)
        if max_value == 0:
            value = 0
        else:
            steps_indices = peakutils.indexes(np.array(buffer_tresholded), thres=0.01 / max_value,
                                              min_dist=int(self.__sampling_rate / 2.0))
            detected_steps = len(steps_indices)*2
            value = detected_steps / self.__processing_window_sec * 60
            self.__steps_total += detected_steps

        self.add_result('steps_per_min', self.__start_time + self.__timestamps[-1], value)
        self.add_result('steps_total', self.__start_time + self.__timestamps[-1], self.__steps_total)

    def __reset(self):
        self.__raw_gyro_values = []
        self.__timestamps = []
        self.__start_time = None

    def __ready_for_processing(self) -> bool:
        return self.__timestamps[-1] - self.__timestamps[0] >= self.__processing_window_sec * 1000
