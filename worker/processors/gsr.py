from typing import Optional

from api.processor import ImproverProcessor


class Processor(ImproverProcessor):
    def __init__(self, sampling_rate=256, processing_window_sec=10.0):
        """
        :param sampling_rate: Sampling rate in Hz
        :param processing_window_sec: Processing window time in seconds
        """
        super().__init__()
        self.__raw_gsr_values = []
        self.__timestamps = []
        self.__sampling_rate = sampling_rate
        self.__processing_window_sec = processing_window_sec
        self.__start_time = None

    def on_data(self, data: dict):
        self.__raw_gsr_values.append(data['GSR_Skin_Resistance'])
        self.__timestamps.append(data['Timestamp'])
        if self.__start_time is None:
            self.__start_time = data['System_Timestamp'] - data['Timestamp']
        if self.__ready_for_processing():
            self.__process()
            self.__reset()

    def get_state(self) -> dict:
        return {
            'start_time': self.__start_time,
            'values_buffer': self.__raw_gsr_values,
            'timestamps_buffer': self.__timestamps
        }

    def restore_state(self, state: Optional[dict]):
        if state is None:
            return
        self.__start_time = state['start_time']
        self.__raw_gsr_values = state['values_buffer']
        self.__timestamps = state['timestamps_buffer']

    def __process(self):
        conductance = [pow(x * 1000, -1) for x in self.__raw_gsr_values]
        avg_skin_conductance = sum(conductance)/len(conductance)
        percent_of_max = avg_skin_conductance / pow(470000, -1) * 100

        self.add_result('conductivity', self.__start_time+self.__timestamps[-1], percent_of_max)

    def __reset(self):
        self.__raw_gsr_values = []
        self.__timestamps = []
        self.__start_time = None

    def __ready_for_processing(self) -> bool:
        return self.__timestamps[-1] - self.__timestamps[0] >= self.__processing_window_sec*1000
