from typing import Optional

from api.processor import ImproverProcessor
from BioSPPy.biosppy.signals import resp


class Processor(ImproverProcessor):
    def __init__(self, sampling_rate=204.8, processing_window_sec=15.0):
        """
        :param sampling_rate: Sampling rate in Hz
        :param processing_window_sec: Processing window time in seconds
        """
        super().__init__()
        self.__raw_resp_values = []
        self.__timestamps = []
        self.__sampling_rate = sampling_rate
        self.__processing_window_sec = processing_window_sec
        self.__start_time = None

    def on_data(self, data: dict):
        if self.__start_time is None:
            self.__start_time = data['System_Timestamp'] - data['Timestamp']
        self.__raw_resp_values.append(data['ECG_RESP_24BIT'])
        self.__timestamps.append(data['Timestamp'])
        if self.__ready_for_processing():
            self.__process()
            self.__reset()

    def get_state(self) -> dict:
        return {
            'values_buffer': self.__raw_resp_values,
            'timestamps_buffer': self.__timestamps,
            'start_time': self.__start_time
        }

    def restore_state(self, state: Optional[dict]):
        if state is None:
            return
        self.__raw_resp_values = state['values_buffer']
        self.__timestamps = state['timestamps_buffer']
        self.__start_time = state['start_time']

    def __reset(self):
        self.__raw_resp_values = []
        self.__timestamps = []
        self.__start_time = None

    def __process(self):
        processed_values = resp.resp(signal=self.__raw_resp_values, sampling_rate=self.__sampling_rate, show=False)
        resp_rate = processed_values['resp_rate']
        ts = processed_values['resp_rate_ts']
        if len(resp_rate) > 0:
            value = sum(resp_rate)/len(resp_rate)
            self.add_result('resp', self.__timestamps[-1] + self.__start_time, value * 60)

    def __ready_for_processing(self) -> bool:
        return self.__timestamps[-1] - self.__timestamps[0] >= self.__processing_window_sec * 1000
