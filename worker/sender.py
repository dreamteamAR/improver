import base64
import socket
import csv
import time
import json
import gzip
import uuid

import requests

REMOTE = 'localhost'
REMOTE_PORT = 8080



class Sender:

    def __init__(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.athlete_id = '2a6afb67-ac6c-4ebe-9f1e-5bbd944df4b6'
        self.map_session = uuid.uuid4().__str__()
        self.session = {
            'id': self.map_session,
            'athleteId': self.athlete_id,
            'startTime': int(time.time() * 1000),
            'details': {
                'type': 'NORMAL',
                'distance': None
            },
            'ended': False
        }

    def run(self):
        self.server.connect((REMOTE, REMOTE_PORT))
        self.send_start_session()
        self.send_data()
        self.send_end_session()

    def send_start_session(self):
        response = requests.post(f'http://{REMOTE}:{REMOTE_PORT}/v1/sessions', json=self.session)
        if response.status_code != 200:
            raise Exception("Cannot create session")

    def send_end_session(self):
        self.session['ended'] = True
        response = requests.put(f'http://{REMOTE}:{REMOTE_PORT}/v1/sessions/{self.session["id"]}', json=self.session)
        if response.status_code != 200:
            raise Exception("Cannot end session")

    def send_data(self):
        self.send_file('2019-10-17_13-50-05_ecg.csv', 'ecg')
        self.send_file('2019-10-17_13-50-05_gsr.csv', 'gsr')
        self.send_file('2019-10-17_13-50-05_imu.csv', 'imu')
        self.send_file('2019-10-17_13-50-05_resp.csv', 'resp')

    def send_file(self, filename: str, metric: str):
        values = []
        with open(filename) as file:
            reader = csv.reader(file)
            header = None
            for val in reader:
                val = val[:-1]
                if header is None:
                    header = val
                else:
                    row = [float(v) for v in val]
                    values.append(row)
                    if len(values) > 1000:
                        self.send_values(metric, header, values)
                        values = []

            self.send_values(metric, header, values)

    def send_values(self, metric: str, headers: list, values):
        values = gzip.compress(json.dumps(values).encode())
        response = requests.post(f'http://{REMOTE}:{REMOTE_PORT}/v1/sessions/{self.map_session}/raw-data', json={
            'metric': metric,
            'headers': headers,
            'values': base64.encodebytes(values).decode()
        })
        if response.status_code != 200:
            raise Exception("Cannot send metrics")

Sender().run()
