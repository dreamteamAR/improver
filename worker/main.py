import base64
import gzip
import importlib
import json
import uuid
from typing import ClassVar
import os
import sys
import faust
from cassandra import DriverException
import asyncio

from api.cassandra_handler import CassandraHandler, DbRawData
from api.processor import ImproverProcessor, Metric

sys.path.append(os.path.join(os.path.dirname(__file__)))


class Data(faust.Record):
    metric: str
    headers: list
    values: bytes

class RawData(faust.Record):
    sessionId: str
    data: Data

app = faust.App(
    'worker',
    broker='kafka://kafka:19092',
    key_serializer='json',
    value_serializer='json',
    store='rocksdb://'
)

processor_classes = {}

cassandra_handler = CassandraHandler(['cassandra'])

topic = app.topic('worker', value_type=RawData)
table = app.Table('test_data_8', default=lambda : "{}", key_type=str, value_type=str, partitions=8)


@app.agent(topic)
async def processA(stream: faust.Stream):
    state = {}
    async for key, value in stream.group_by(RawData.sessionId).items():
        metrics = []
        raw_data = []
        try:
            raw, metrics = await process(value, state, table)
            metrics.extend(metrics)
            raw_data.append(cassandra_handler.insert_raw_data(raw))
            print(f'A: {value.sessionId} {value.data.metric}')
        except DriverException as e:
            print(f'A: {value.sessionId} {value.data.metric} {e.with_traceback(None)}')
        await asyncio.gather(cassandra_handler.insert_metrics(metrics), *raw_data)


async def process(raw_data: RawData, state: dict, table: faust.tables.TableT):
    shared_state: dict = json.loads(table[raw_data.sessionId])
    # prepare processing class stub
    processor_class = import_processor_class_cached(raw_data.data.metric)
    processor: ImproverProcessor = processor_class()
    stored_data: dict = state.get(raw_data.sessionId, {})

    # add metric family to session if it doesn't exist yet
    if 'metric_families' not in stored_data:
        stored_data['metric_families'] = await cassandra_handler.get_session_metric_families(uuid.UUID(raw_data.sessionId))
    if raw_data.data.metric not in stored_data['metric_families']:
        stored_data['metric_families'].append(raw_data.data.metric)
        await cassandra_handler.update_session_metric_families(uuid.UUID(raw_data.sessionId), stored_data['metric_families'])

    # restore state of processor
    if raw_data.data.metric in stored_data:
        processor.restore_state(stored_data.get(raw_data.data.metric))



    # decompress data
    blob = base64.b64decode(raw_data.data.values)
    values = json.loads(gzip.decompress(blob))

    # process data
    for value in values:
        row = dict(zip(raw_data.data.headers, value))
        processor.on_data(row)

    # collect results from processor
    results = processor.get_results()

    # save state for future processing of next batch
    stored_data[raw_data.data.metric] = processor.get_state()
    state[raw_data.sessionId] = stored_data

    shared_state[raw_data.data.metric] = shared_state.get(raw_data.data.metric, -1) + 1
    # insert metrics into db
    table[raw_data.sessionId] = json.dumps(shared_state)
    store_in_db = DbRawData(uuid.UUID(raw_data.sessionId), raw_data.data.metric, shared_state[raw_data.data.metric], blob)
    return store_in_db, [Metric.new(uuid.UUID(raw_data.sessionId), res) for res in results]


def import_processor_class_cached(processor: str) -> ClassVar[ImproverProcessor]:
    if processor in processor_classes:
        return processor_classes[processor]
    else:
        cls = getattr(importlib.import_module(f'processors.{processor}'), 'Processor')
        processor_classes[processor] = cls
        return cls



