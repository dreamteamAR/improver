import 'hammerjs';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import {Chart} from 'chart.js';

if (environment.production) {
  enableProdMode();
}

const horizontalLinePlugin = {
  afterDraw(chartInstance) {
    let yValue;
    const yScale = chartInstance.scales['y-axis-0'];
    const canvas = chartInstance.chart;
    const ctx = canvas.ctx;
    let index;
    let line;
    let style;

    if (chartInstance.options.horizontalLine) {
      for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
        line = chartInstance.options.horizontalLine[index];

        if (!line.style) {
          style = 'rgba(169,169,169, .6)';
        } else {
          style = line.style;
        }

        if (line.y) {
          yValue = yScale.getPixelForValue(line.y);
        } else {
          yValue = 0;
        }

        ctx.lineWidth = 1;

        if (yValue) {
          ctx.beginPath();
          ctx.moveTo(37, yValue);
          ctx.lineTo(canvas.width, yValue);
          ctx.strokeStyle = style;
          ctx.stroke();
        }

        if (line.text) {
          ctx.fillStyle = style;
          ctx.fillText(line.text, 40, yValue + ctx.lineWidth + 10);
        }
      }
      return;
    }
  }
};
Chart.pluginService.register(horizontalLinePlugin);

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
