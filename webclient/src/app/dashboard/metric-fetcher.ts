import {interval, Subscription} from 'rxjs';
import {MetricsService} from '../metrics/metrics.service';

export class MetricFetcher {
  public current: {
    max: number,
    avg: number,
    min: number,
    timestamp: number
  };
  private subscription: Subscription;
  constructor(private sessionId: string, private metric: string, private metricsService: MetricsService) {
    this.current = {
      max: 0,
      avg: 0,
      min: 0,
      timestamp: 0
    };
    this.fetchMetric();
    this.subscription = interval(5000).subscribe(x => this.fetchMetric());
  }

  public free() {
    this.subscription.unsubscribe();
  }

  public fetchMetric() {
    this.metricsService.getMetrics(this.sessionId, this.metric, 1, this.current.timestamp).subscribe(
      metrics => {
        if(metrics.length > 0) {
          metrics = metrics.sort((a, b) => b.timestamp - a.timestamp);
          this.current = metrics[0];
        }
      }
    );
  }



}
