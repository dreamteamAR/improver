import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {MetricFetcher} from '../metric-fetcher';
import {MetricsService} from '../../metrics/metrics.service';
import {Athlete} from '../../athletes/athlete';
import {Session} from "../../sessions/session";

@Component({
  selector: 'app-current-key-metrics',
  templateUrl: './current-key-metrics.component.html',
  styleUrls: ['./current-key-metrics.component.scss']
})
export class CurrentKeyMetricsComponent implements OnInit, OnDestroy, OnChanges {

  @Input()
  private athlete: Athlete;
  @Input()
  private session: Session;

  private stepsMetricFetcher: MetricFetcher;
  private bpmMetricFetcher: MetricFetcher;
  private sweatingMetricFetcher: MetricFetcher;

  constructor(private metricsService: MetricsService) { }

  ngOnInit() {
    this.initializeFetchers();
  }

  ngOnDestroy() {
    this.freeFetchers();
  }

  BPMSectionName(hr: number): string {
    const hrMax = 207 - (0.7 * (new Date().getFullYear() - new Date(this.athlete.birthDate).getFullYear()));
    if (hr != null) {
      if (hr > hrMax) {
        return 'Over MAX';
      } else if (hr > hrMax * 0.9) {
        return 'Very Hard';
      } else if (hr > hrMax * 0.8) {
        return 'Hard';
      } else if (hr > hrMax * 0.7) {
        return 'Moderate';
      } else if (hr > hrMax * 0.6) {
        return 'Light';
      } else {
        return 'Rest';
      }
    }
    return '';
  }

  initializeFetchers() {
    this.stepsMetricFetcher = new MetricFetcher(this.session.key.id, 'steps_total', this.metricsService);
    this.bpmMetricFetcher = new MetricFetcher(this.session.key.id, 'bpm', this.metricsService);
    this.sweatingMetricFetcher = new MetricFetcher(this.session.key.id, 'conductivity', this.metricsService);
  }

  freeFetchers() {
    if(this.stepsMetricFetcher) {
      this.stepsMetricFetcher.free();
      this.bpmMetricFetcher.free();
      this.sweatingMetricFetcher.free();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.freeFetchers();
    this.initializeFetchers();
  }

}
