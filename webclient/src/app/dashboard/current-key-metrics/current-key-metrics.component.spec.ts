import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentKeyMetricsComponent } from './current-key-metrics.component';

describe('CurrentKeyMetricsComponent', () => {
  let component: CurrentKeyMetricsComponent;
  let fixture: ComponentFixture<CurrentKeyMetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentKeyMetricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentKeyMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
