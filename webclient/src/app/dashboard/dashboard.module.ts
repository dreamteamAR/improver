import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImproverPlotsComponent } from './improver-plots/improver-plots.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { ChartsModule } from 'ng2-charts';
import { MetricsModule } from '../metrics/metrics.module';
import { SessionsModule } from '../sessions/sessions.module';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import {MatDividerModule} from '@angular/material/divider';
import {MatTabsModule} from "@angular/material/tabs";
import {AthletesModule} from "../athletes/athletes.module";
import { CurrentKeyMetricsComponent } from './current-key-metrics/current-key-metrics.component';
import { SummaryComponent } from './summary/summary.component';



@NgModule({
  declarations: [ImproverPlotsComponent, CurrentKeyMetricsComponent, SummaryComponent],
  imports: [
    CommonModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    LayoutModule,
    ChartsModule,
    MetricsModule,
    FormsModule,
    SessionsModule,
    MatButtonToggleModule,
    MatDividerModule,
    MatTabsModule,
    AthletesModule
  ],
  exports: [
    ImproverPlotsComponent
  ]
})
export class DashboardModule { }
