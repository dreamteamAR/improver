import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Session} from '../../sessions/session';
import {MetricsService} from '../../metrics/metrics.service';
import {Athlete} from '../../athletes/athlete';
import {ChartType} from 'chart.js';
import {MultiDataSet} from 'ng2-charts';
import {interval} from "rxjs";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit, OnChanges {

  private barChartOptions = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    showTooltips: true,
    hover: {
      animationDuration: 0 // duration of animations when hovering an item
    },
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
          var allData = data.datasets[tooltipItem.datasetIndex].data;
          var tooltipLabel = data.labels[tooltipItem.index];
          var tooltipData = allData[tooltipItem.index];
          var total = 0;
          for (var i in allData) {
            total += allData[i];
          }
          var tooltipPercentage = Math.round((tooltipData / total) * 100);
          return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
        }
      }
    },
    horizontalLine: [],
    responsiveAnimationDuration: 0, // animation duration after a resize
  };

  public colors = [{
    backgroundColor: [
      'rgb(255, 0, 0)',
      'rgb(255, 168, 0)',
      'rgb(255, 255, 0)',
      'rgb(160, 160, 255)'
    ]
  }];

  @Input()
  private session: Session;

  @Input()
  private athlete: Athlete;

  constructor(private metricsService: MetricsService) { }

  private labels: string[] = [
    'Max',
    'Very hard',
    'Hard',
    'Moderate'
  ];
  private doughnutChartType: ChartType = 'doughnut';

  private valueHolder: ValueHolder = new ValueHolder();

  private data: MultiDataSet = [];

  ngOnInit() {
    this.calculateData();
  }
  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    this.calculateData();
  }
  calculateData() {
    this.metricsService.getMetrics(this.session.key.id, 'bpm', 1).subscribe(metrics => {
      const hrMax = 207 - (0.7 * (new Date().getFullYear() - new Date(this.athlete.birthDate).getFullYear()));
      this.valueHolder = new ValueHolder();
      metrics.forEach(metric => {
        if (metric.max > hrMax) {
          this.valueHolder.max++;
        } else if (metric.max > hrMax * 0.9) {
          this.valueHolder.veryHard++;
        } else if (metric.max > hrMax * 0.8) {
          this.valueHolder.hard++;
        } else  {
          this.valueHolder.moderate++;
        }
      });
      this.data = this.valueHolder.data;
    });
  }
}

class ValueHolder {
  max = 0;
  veryHard = 0;
  hard = 0;
  moderate = 0;

  get data() {
    return [[this.max, this.veryHard, this.hard, this.moderate]];
  }
}
