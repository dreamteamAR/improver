import {ApplicationRef, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { SessionsService } from 'src/app/sessions/sessions.service';
import { Session } from 'src/app/sessions/session';
import {HorizontalLine, MetricInfo} from '../../metrics/metric-card/metric-card.component';
import {ActivatedRoute} from '@angular/router';
import {AthletesService} from '../../athletes/athletes.service';
import {Athlete} from '../../athletes/athlete';

@Component({
  selector: 'app-improver-plots',
  templateUrl: './improver-plots.component.html',
  styleUrls: ['./improver-plots.component.scss']
})
export class ImproverPlotsComponent implements OnInit {

  public aggregations = [
    { value: 5000, display: '5s' },
    { value: 30000, display: '30s' },
    { value: 60000, display: '1m' },
    { value: 300000, display: '5m' },
    { value: 600000, display: '10m' }
  ];
  public selectedAggregation = 5000;
  public session: Session;
  public sessions: Session[] = [];
  private breakpoint: number;
  private athlete: Athlete;
  private ecgMetrics: MetricInfo[] = [];
  private otherMetrics: MetricInfo[] = [
    new MetricInfo('Steps per minute', 'steps_per_min'),
    new MetricInfo('Skin conductivity', 'conductivity')
  ];


  constructor(
    private breakpointObserver: BreakpointObserver,
    private sessionsService: SessionsService,
    private route: ActivatedRoute,
    private athletesService: AthletesService
  ) {

  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.athletesService.getAthleteById(id).subscribe(athlete => {
      this.athlete = athlete;
      const hrMax = 207 - (0.7 * (new Date().getFullYear() - new Date(athlete.birthDate).getFullYear()));
      this.ecgMetrics = [
        new MetricInfo('Beats per minute', 'bpm', [
          new HorizontalLine( hrMax, 'rgb(255, 0, 0)', 'MAX'),
          new HorizontalLine( hrMax * 0.9, 'rgb(255, 168, 0)', 'VERY HARD'),
          new HorizontalLine( hrMax * 0.8, 'rgb(255, 255, 0)', 'HARD'),
          new HorizontalLine( hrMax * 0.7, 'rgb(160, 160, 255)', 'MODERATE')
        ]),
        new MetricInfo('Heart rate variability', 'hrv'),
        new MetricInfo('Respiratory rate', 'resp')
      ];
      if (this.athlete != null) {
        this.fetchSessions();
      }
    });
    this.breakpoint = (window.innerWidth <= 600) ? 1 : 2;
  }

  onSessionChange(session) {
    this.session = session;
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 600) ? 1 : 2;
  }

  compareSessions(session1: Session, session2: Session): boolean {
    return session1 && session2 ? session1.key.id === session2.key.id : session1 === session2;
  }

  private fetchSessions() {
    this.sessionsService.getSessionsByAthleteId(this.athlete.id).subscribe((sessions) => {
      this.sessions = sessions.sort((first, second) => second.startTime.getTime() - first.startTime.getTime());
      if (this.session == null && sessions.length > 0) {
        this.session = this.sessions[0];
      }
    });

  }
}
