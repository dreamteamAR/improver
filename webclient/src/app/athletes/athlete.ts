export class Athlete {
  id: string;
  firstName: string;
  lastName: string;
  imageUrl: string;
  weight: number;
  height: number;
  birthDate: Date;
  gender: string;
}
