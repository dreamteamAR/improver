import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AthleteInformationComponent } from './athlete-information/athlete-information.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatButtonModule} from "@angular/material/button";
import {MatListModule} from "@angular/material/list";
import { AthleteListComponent } from './athlete-list/athlete-list.component';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [AthleteInformationComponent, AthleteListComponent],
  exports: [
    AthleteInformationComponent
  ],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatButtonModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatIconModule,
    RouterModule
  ]
})
export class AthletesModule { }
