import {Component, Input, OnInit} from '@angular/core';
import {AthletesService} from '../athletes.service';
import {Athlete} from '../athlete';

@Component({
  selector: 'app-athlete-information',
  templateUrl: './athlete-information.component.html',
  styleUrls: ['./athlete-information.component.scss']
})
export class AthleteInformationComponent implements OnInit {

  @Input()
  private athlete: Athlete;
  constructor() { }

  ngOnInit() {

  }

}
