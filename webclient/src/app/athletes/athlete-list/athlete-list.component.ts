import { Component, OnInit } from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {AthletesService} from '../athletes.service';
import {Athlete} from '../athlete';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-athlete-list',
  templateUrl: './athlete-list.component.html',
  styleUrls: ['./athlete-list.component.scss']
})
export class AthleteListComponent implements OnInit {

  private breakpoint: number;
  private athletes: Athlete[];
  constructor(private breakpointObserver: BreakpointObserver, private athletesService: AthletesService) { }

  ngOnInit() {
    this.calcBreakpoint();
    this.athletesService.getAthletes().subscribe( athletes => this.athletes = athletes);
  }

  calcBreakpoint() {
    this.breakpoint = (window.innerWidth <= 1800) ? 3 : 4;
    if (this.breakpoint === 3) {
      this.breakpoint = (window.innerWidth <= 800) ? 2 : 3;
    }
    if (this.breakpoint === 2) {
      this.breakpoint = (window.innerWidth <= 500) ? 1 : 2;
    }
  }

  onResize(event) {
    this.calcBreakpoint();
  }

}
