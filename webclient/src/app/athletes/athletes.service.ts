import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Athlete} from './athlete';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AthletesService {

  athletesEndpoint = environment.host + '/v1/athletes';

  constructor(private httpClient: HttpClient) { }

  public getAthletes(): Observable<Athlete[]> {
    return this.httpClient.get<Athlete[]>(this.athletesEndpoint);
  }

  public getAthleteById(id: string): Observable<Athlete> {
    return this.httpClient.get<Athlete>(`${this.athletesEndpoint}/${id}`);
  }
}
