import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Session } from './session';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionsService {
  sessionsEndpoint = environment.host + '/v1/sessions';
  constructor(private httpClient: HttpClient) { }

  public getSessions(): Observable<Session[]> {
    return this.httpClient.get<Session[]>(this.sessionsEndpoint).pipe(tap( sessions => {
      sessions.forEach(session => {
      session.startTime = new Date(session.startTime);
      if(session.endTime === 0) {
        session.endTime = null;
      }
      });
    }));
  }
  public getSessionsByAthleteId(id: string): Observable<Session[]> {
    return this.httpClient.get<Session[]>(environment.host + `/v1/athletes/${id}/sessions`).pipe(tap( sessions => {
      sessions.forEach(session => {
        session.startTime = new Date(session.startTime);
        if(session.endTime === 0) {
          session.endTime = null;
        }
      });
    }));
  }
}
