export class Session {
    key: {
      id: string,
      athleteId: string,
    };
    startTime: Date;
    details: {
        type: string,
        distance: number
    };
    endTime: number;
}
