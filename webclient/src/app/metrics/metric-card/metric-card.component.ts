import {Component, OnInit, Input, OnDestroy, Output, OnChanges} from '@angular/core';
import { MetricsService } from '../metrics.service';
import {interval, Subscription} from 'rxjs';
import 'chartjs-plugin-zoom';
import {Chart} from 'chart.js';
import {Session} from '../../sessions/session';

@Component({
  selector: 'app-metric-card',
  templateUrl: './metric-card.component.html',
  styleUrls: ['./metric-card.component.scss']
})
export class MetricCardComponent implements OnInit, OnDestroy, OnChanges  {

  constructor(private metricsService: MetricsService) {

  }

  @Input()
  public barChartType = 'line';
  @Input()
  public barChartLegend = true;

  @Input()
  private metricInfo: MetricInfo;
  @Input()
  private session: Session;
  @Input()
  private cycle: number;
  @Input()
  private ;

  private labels: Date[] = [new Date(0)];
  private chartSeries: ChartSeries = new ChartSeries();
  private subscription: Subscription = null;
  private barChartOptions = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    showTooltips: true,
    hover: {
      animationDuration: 0 // duration of animations when hovering an item
    },
    horizontalLine: [],
    responsiveAnimationDuration: 0, // animation duration after a resize
    scales: {
      xAxes: [{
        type: 'time',
        time: {
          displayFormats: {
            quarter: 'MMM YYYY'
          }
        }
      }]
    },
    plugins: {
      zoom: {
        pan: {
          enabled: true,
          mode: 'x',
          speed: 10,
          threshold: 10
        },
        zoom: {
          enabled: true,
          mode: 'x'
        }
      }
    }
  };

  ngOnInit() {
    this.barChartOptions.horizontalLine = this.metricInfo.horizontalLines;
    this.fetchMetricData();
    if (this.session.endTime == null) {
      this.subscription = interval(5000).subscribe(x => this.fetchMetricData());
    }
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    this.barChartOptions.horizontalLine = this.metricInfo.horizontalLines;
    this.fetchMetricData();
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    if (this.session.endTime == null) {
      this.subscription = interval(5000).subscribe(x => this.fetchMetricData());
    }
  }

  private fetchMetricData() {
    this.metricsService.getMetrics(this.session.key.id, this.metricInfo.metric, this.cycle).subscribe((metrics) => {
      this.labels = [];
      this.chartSeries.clear();
      metrics.forEach( value => {
            this.labels.push(new Date(value.timestamp));
            this.chartSeries.min.data.push(value.min);
            this.chartSeries.avg.data.push(value.avg);
            this.chartSeries.max.data.push(value.max);
        }
      );
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}

class ChartSeries {
  public max: ChartData = new ChartData('Max');
  public avg: ChartData = new ChartData('Average');
  public min: ChartData = new ChartData('Min');
  public data: ChartData[] = [this.max, this.avg, this.min];
  clear() {
    this.max.data = [];
    this.avg.data = [];
    this.min.data = [];
  }
}

class ChartData {
  public data: number[] = [1];
  public label: string;

  constructor(label: string) {
    this.label = label;
  }
}

export class MetricInfo {
  constructor(public title: string, public metric: string, public horizontalLines: HorizontalLine[] = []) {

  }
}

export class HorizontalLine {
  constructor(public y: number, public style: string, public text: string) {}
}
