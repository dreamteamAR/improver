import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Metric } from './metric';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MetricsService {

  sessionEndpoint = environment.host + '/v1/sessions';

  constructor(private httpClient: HttpClient) { }

  public getMetricNames(sessionId: string): Observable<string[]>  {
    return this.httpClient.get<string[]>(this.sessionEndpoint + '/' + sessionId + '/metrics');
  }

  public getMetrics(sessionId: string, metric: string, cycle: number = 1000, start: number = 0): Observable<Metric[]> {
    return this.httpClient.get<Metric[]>(`${this.sessionEndpoint}/${sessionId}/metrics/${metric}?cycle=${cycle}&start=${start}`);
  }
}
