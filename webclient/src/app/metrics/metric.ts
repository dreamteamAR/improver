export class Metric {
    min: number;
    avg: number;
    max: number;
    timestamp: number;
}
