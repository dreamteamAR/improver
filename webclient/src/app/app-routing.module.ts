import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImproverPlotsComponent } from './dashboard/improver-plots/improver-plots.component';
import {AthleteListComponent} from './athletes/athlete-list/athlete-list.component';


const routes: Routes = [
  { path: 'athlete/:id', component: ImproverPlotsComponent },
  { path: 'home', component: AthleteListComponent},
  { path: '',  pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
